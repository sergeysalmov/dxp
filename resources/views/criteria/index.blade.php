@extends(( $layout ? 'layouts.app' : 'layouts.ajax' ))
@section('title', $diagnosis->name)
@section('content')
<style>
.art-content .art-postcontent-0 .layout-item-18 { color: #0B0D0F; background: #F7F7F8 url('/css/images/9ddb1.png') scroll;background: rgba(247, 247, 248, 0.6) url('/css/images/9ddb1.png') scroll; padding: 0px; vertical-align: middle;  }
.ie7 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
.ie6 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
</style>
<input type="hidden" id="next-criterion" value="{{$next_criterion}}">
<input type="hidden" id="criterion-page" value="{{$criterion_page}}">
<input type="hidden" id="diagnosis_id" value="{{$breadcrumbs_str}}">
<input type="hidden" id="return_to" value="{{$return_to}}">
<input type="hidden" id="return_type" value="{{$return_type}}">
<input type="hidden" id="nested_return_to" value="{{$nested}}">
<div class="art-sheet clearfix">
    <div class="art-layout-wrapper">
        <div class="art-content-layout">
            <div class="art-content-layout-row">
                <div class="art-layout-cell art-content">
                    <article class="art-post art-article">
                        <div class="art-postcontent art-postcontent-0 clearfix">
                            <div class="art-content-layout-wrapper layout-item-0">
                                <div class="art-content-layout layout-item-1">
                                    <div class="art-content-layout-row">
                                        <div class="art-layout-cell top-page">
                                            <p class = "diagnosis-name breadcrumbs">
                                                <span>{!! Helper::get_breadcrumbs($breadcrumbs) !!}</span>
                                            </p>
                                            <p class = diagnosis-name-top>
                                                <span>{{$diagnosis->name}}</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="art-content-layout-wrapper layout-criteria-title-div">
                                <div class="art-content-layout criteria-title-div">
                                    <div class="art-content-layout-row">
                                        <div class="art-layout-cell">
                                            <h2><span class = "criteria-title">{{$present == 'all' ? 'Criteria' : 'Criterion'}}</span></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="art-content-layout-br criteria-master-divider"></div>
                            @php
                            $criterion_id = key($nested_criteria);
                            $note = ($present == 'all' ? $diagnosis->note : App\Criteria::get_note($criterion_id));
                            $info = ($present == 'all' ? $diagnosis->info : App\Criteria::get_info($criterion_id));
                            $letter = !empty(current($nested_criteria)['children']) ? true : App\Criteria::show_letter($diagnosis);
                            $multi_levels=Helper::count_r($nested_criteria);
                            //var_dump(Request::fullUrl());
                            //var_dump(url()->current());
                            $x= Request::fullurl();
                            @endphp
                            {!! Helper::render_criteria($nested_criteria,0,$criterion_page,$letter,$multi_levels,$x,$breadcrumbs) !!}
                            <div class="art-content-layout-br criteria-master-divider"></div>
                            @include('general._menu', ['page' => 'criteria','favorite_present' => $present,'note' =>$note, 'info' => $info])
                            <div class="art-content-layout layout-item-16">
                                <div class="art-content-layout-row">
                                    <div class="art-layout-cell layout-item-18" style="width: 100%" >
                                        <p style="text-align: center;">
                                            <a class="art-button" id = "continue-btn">Continue</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</div>
@if(Helper::other_criterion($nested_criteria))
    @include('criteria._other',['diagnosis_name' => $diagnosis->name, 'free_text' => current($nested_criteria)['free_text']])
@endif

@stop