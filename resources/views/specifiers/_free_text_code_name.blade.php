<style>
.art-content .art-postcontent-0 .layout-item-5 { margin-top: 10px;margin-right: 20px;margin-bottom: 10px;margin-left: 20px;  }
.art-content .art-postcontent-0 .layout-item-6 { border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-width:0px;border-color:#E1E8EF; color: #111418; background: ; border-spacing: 27px 0px; border-collapse: separate; border-radius: 0px;  }
.art-content .art-postcontent-0 .layout-item-7 { color: #323B43; background: ; padding: 5px; vertical-align: top; border-radius: 0px;  }
.art-content .art-postcontent-0 .layout-item-8 { margin-top: 10px;margin-right: 20px;margin-bottom: 20px;margin-left: 20px;  }
.art-content .art-postcontent-0 .layout-item-9 { border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-width:0px;border-color:#E1E8EF; color: #111418; background: ; border-spacing: 7px 0px; border-collapse: separate; border-radius: 0px;  }
.art-content .art-postcontent-0 .layout-item-10 { border-style:Double;border-width:3px;border-color:#D8DEE4; color: #303841; background: #F7F7F8 url('/css/images/a64d4.png') scroll; padding: 3px; vertical-align: top; border-radius: 0px;  }
.art-content .art-postcontent-0 .layout-item-11 { border-style:Double;border-width:3px;border-color:#D8DEE4; color: #303841; background: #F7F7F8 url('/css/images/a64d4.png') scroll; padding: 3px; vertical-align: top; border-radius: 0px;  }
.ie7 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
.ie6 .art-post .art-layout-cell {border:none !important; padding:0 !important; }

</style>
<div class="art-content-layout-wrapper layout-item-5">
    <div class="art-content-layout layout-item-6">
        <div class="art-content-layout-row">
            <div class="art-layout-cell layout-item-7" style="width: 100%" >
                <p class="MsoNormal">
                    <span style="font-size: 14px;">{{$specify->text}}</span>
                </p>
            </div>
        </div>
    </div>
</div>
<div class = "lines_container">
@php
$s_var = $specify->variable;
$params = (empty($variables->$s_var) ? null : $variables->$s_var);
$variables_input = Helper::free_text_values($params);
@endphp
@foreach($variables_input as $key=>$val)

   <div id = "{{$key}}" class="line-item art-content-layout-wrapper layout-item-8">
    <div class="art-content-layout layout-item-9">
        <div class="code_name art-content-layout-row">
            <div class="code-div art-layout-cell layout-item-10" style="width: 33%" >
                <p><span style="font-size: 14px; color: #0E141B;">Enter code:<input type="text" class="code" value="{{empty($val->code) ? '' : $val->code}}"></span><br></p>
            </div>
            <div class="name-div art-layout-cell layout-item-11" style="width: 67%" >
                <p><span style="font-size: 14px; color: #0E141B;">Enter name:<br><input type="text" class="name" value="{{empty($val->name) ? '' : $val->name}}"></span><br></p>
            </div>

            
        </div>
        @if(!$loop->first)
        <div class="code_name art-content-layout-row"><div class="art-layout-cell"><p style="margin:0px;"><a href="" class="remove-line" style="font-size: 12px;">Remove</a></p></div></div>
        @endif
    </div>
    </div>
@endforeach
</div>
@unless(!empty($variables->delirium_whether))
<div class="add-line art-content-layout-wrapper layout-item-8">
    <div class="art-content-layout layout-item-9">
        <div class="art-content-layout-row">
            <div class="art-layout-cell layout-item-7" style="width: 100%" >
                <p><span style="color: #0E141B;"><a id="add-line" class="art-button" title="Add another associated medical or genetic condition or environmental factor.">Add</a>&nbsp;</span></p>
            </div>
        </div>
    </div>
</div>
@endunless

 <script type="text/javascript">
    var variable = $("#variable").val();
    // var arr = JSON.parse($("#specify-values").val());
    var arr = JSON.parse($("#variables").val());
    var n;
    // var  btn_value = {'var_data': arr['item_id']};
    // var  btn_value = {'var_data': arr[variable]["var_data"]};
    // delete arr[variable]["var_data"];
    $(document).ready(function(){
        n=$(".line-item").length;
        if(n>=5)
            $( "#add-line" ).remove();

    })
    $(document).on('keyup', "input.code,input.name", function(){
        console.log($(this).closest(".line-item").attr("id"))
        input = $(this).attr("class");
        if(jQuery.isEmptyObject(arr[variable]))
            arr[variable]={};
        if (jQuery.isEmptyObject(arr[variable][$(this).closest(".line-item").attr("id")]))
            arr[variable][$(this).closest(".line-item").attr("id")] = {};
        arr[variable][$(this).closest(".line-item").attr("id")][input]= $(this).val();
        // $("#specify-values").val(JSON.stringify(Object.assign(btn_value, arr)));
        // arr[variable] = Object.assign(btn_value, arr[variable])
        $("#variables").val(JSON.stringify(arr));
    })

    $(document).off('click', "#add-line").on('click', "#add-line", function(){
        var div = $('div[id^="free-text-"]:last');
        var num = parseInt( div.prop("id").match(/\d+/g), 10 ) +1;
        var klon = div.clone().prop('id', 'free-text-'+num );
        $(klon).find("input").val("");
        if(num=="2")
            $(klon).find(".code_name").after('<div class="code_name art-content-layout-row"><div class="art-layout-cell"><p style="margin:0px;"><a class="remove-line" style="font-size: 12px;">Remove</a></p></div></div>');
        $( klon ).appendTo( ".lines_container" );
        n++;
        if(n>=5)
            $( "#add-line" ).remove();

    });
    
    $(document).off('click', ".remove-line").on('click', ".remove-line", function(e){
        e.preventDefault();
        row = $( this ).closest( ".line-item" );
        if(arr[variable])
            delete arr[variable][$(row).attr('id')];
        $("#variables").val(JSON.stringify( arr));
        $(row).remove();
        n--;
        if(n<5 && !($("#add-line").length)){
            add_btn = '<a id="add-line" class="art-button" title="Add another associated medical or genetic condition or environmental factor.">Add</a>';
            span = $(".add-line").find("span");
            $(span).append(add_btn);
        }
    });


</script>