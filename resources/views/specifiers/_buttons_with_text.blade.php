<style>
.art-content .art-postcontent-0 .layout-item-5 { margin-top: 20px;margin-bottom: 0px;  }
.art-content .art-postcontent-0 .layout-item-6 { border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-width:0px;border-top-color:#9FB4CB;border-right-color:#9FB4CB;border-bottom-color:#9FB4CB;border-left-color:#9FB4CB; color: #111418; border-spacing: 30px 0px; border-collapse: separate;  }
.art-content .art-postcontent-0 .layout-item-7 { border-top-width:2px;border-top-style:Ridge;border-top-color:#7493B4;margin-top: 20px;margin-right: 20px;margin-bottom: 10px;margin-left: 20px;  }
.art-content .art-postcontent-0 .layout-item-8 { color: #111418; padding-right: 10px;padding-left: 10px; vertical-align: top;  }
.art-content .art-postcontent-0 .layout-item-9 { border-style:Double;border-width:3px;border-color:#D8DEE4; color: #0B0D0F; background: #F7F7F8; padding: 5px; vertical-align: middle; border-radius: 10px;  }
.ie7 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
.ie6 .art-post .art-layout-cell {border:none !important; padding:0 !important; }

</style>
@php
($s_var = $specify->variable);
@endphp
<div class="art-content-layout-wrapper layout-item-5">
    <div class="art-content-layout layout-item-6">
        <div class="art-content-layout-row">
            <div class="art-layout-cell layout-item-8" style="width: 50%" >
                <p>
                    <span style="-webkit-border-horizontal-spacing: 10px;">
                        <span style="font-size: 18px; font-style: italic;">
                            <span style="color: rgb(45, 63, 83);">{{$specify->text}}</span>
                        </span>
                    </span>
                    <span style="color: rgb(50, 59, 67);"><br></span>
                </p>
            </div>
            <div class="specifiers-answers art-layout-cell layout-item-9" style="width: 50%" >
                @foreach($specifiers_items as $key => $item)
                @php 
                    $class_a = "art-button";
                    $checked = '';
                    if(in_array($item->var_data, $variables->$s_var ?? [])){
                        $class_a .=" chosen";
                        $checked = "checked";
                    }
                @endphp
                <p style="text-align: left; padding-left: 20px; line-height: 200%;">
                    <span style="color: rgb(48, 56, 65); -webkit-border-horizontal-spacing: 10px;">
                        <span style="-webkit-border-horizontal-spacing: 27px;">
                            <a id="{{$item->id}}" class="{{$class_a}}" var_data="{{$item->var_data}}">
                                <span style="font-family: Arial, 'Arial Unicode MS', Helvetica, sans-serif;">{{$item->btn_text}}</span>
                            </a>
                            <input type="checkbox" name="items" value="{{$item->id}}" class ="hidden-input">
                        </span><br>
                    </span>
                </p>
                @endforeach
            </div>
        </div>
    </div>
</div>