<style>
.art-content .art-postcontent-0 .layout-item-5-1 { margin-top: 5px;margin-right: 20px;margin-bottom: 15px;margin-left: 20px;  }
.art-content .art-postcontent-0 .layout-item-6-1 { border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-width:0px;border-top-color:#9FB4CB;border-right-color:#9FB4CB;border-bottom-color:#9FB4CB;border-left-color:#9FB4CB; color: #111418; border-spacing: 5px 0px; border-collapse: separate;  }
.art-content .art-postcontent-0 .layout-item-8-1 { color: #0B0D0F; background: ; padding-right: 10px;padding-left: 10px; vertical-align: top;  }
.art-content .art-postcontent-0 .layout-item-5 { margin-top: 15px;margin-right: 20px;margin-bottom: 15px;margin-left: 20px;  }
.art-content .art-postcontent-0 .layout-item-6 { border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-width:0px;border-top-color:#9FB4CB;border-right-color:#9FB4CB;border-bottom-color:#9FB4CB;border-left-color:#9FB4CB; color: #111418; border-spacing: 5px 0px; border-collapse: separate;  }
.art-content .art-postcontent-0 .layout-item-7 { border-top-width:2px;border-top-style:Solid;border-top-color:#98AFC8;margin-top: 10px;margin-right: 30px;margin-bottom: 10px;margin-left: 30px;  }
.art-content .art-postcontent-0 .layout-item-8 { color: #0B0D0F; background: #F5F5FA url('/css/images/b3e84.png') scroll; padding-right: 10px;padding-left: 10px; vertical-align: top;  }
.art-content .art-postcontent-0 .layout-item-9 { color: #111418; padding-right: 10px;padding-left: 10px; vertical-align: top;  }
.art-content .art-postcontent-0 .layout-item-10 { border-style:Double;border-width:3px;border-color:#D8DEE4; color: #0B0D0F; background: #F7F7F8 url('/css/images/23908.png') scroll; padding: 0px; vertical-align: top; border-radius: 5px;  }
.ie7 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
.ie6 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
</style>
<div class="columns_present specifiers-answers">
@php
$disorder_name = '';
$path = '';
$substance = false;
$s_var = $specify->variable;
if($specify->behavior && $specify->behavior->name == 'substance_use_disorder'){
    $substance = true;
    $substance_class = ($variables->substance_class ?? '');
    $disorder_name = $specify->get_use_disorder($substance_class);
    $name_path= $specify->get_name_path($disorder_name);
    $path = $specify->get_id_by_name($disorder_name);
}
@endphp
@if(!empty($specify->text))
<div class="art-content-layout-wrapper layout-item-5-1">
<div class="art-content-layout layout-item-6-1">
    <div class="art-content-layout-row">
    <div class="art-layout-cell layout-item-8-1" style="width: 100%" >
        <p><span style="font-size: 14px; -webkit-border-horizontal-spacing: 10px; color: #323D48;">{{$specify->text}}</span><br></p>
    </div>
    </div>
</div>
</div>
<div class="art-content-layout-br layout-item-7"></div>
@endif
@foreach($specifiers_items as $key => $item)
@if(!$loop->first)
    <div class="art-content-layout-br layout-item-7"></div>
@endif
<div class="art-content-layout-wrapper layout-item-5">
    <div class="art-content-layout layout-item-6">
        <div class="art-content-layout-row">
            <div class="art-layout-cell layout-item-8" style="width: 25%" >
                <p>
                    <span style="color: rgb(45, 63, 83); font-style: italic; font-weight: bold;">{{$item->columnA}}</span><br>
                </p>
            </div>
            <div class="art-layout-cell layout-item-9" style="width: 60%" >
                <p>
                    <span style="font-size: 14px; color: #0E141B;">{{$item->columnB}}</span>
                    @if($substance && $item->btn_text=='With')
                        <span class="diagnosis-name"><a id="{{$name_path}}" class="substance-class" href="/diagnosis/{{$path}}?return_to={{Request::path()}}&return_type=substance" style="font-size: 14px; color:#FF5F0F;">{{$disorder_name}}</a></span>
                    @endif
                </p>
            </div>
            <div class="art-layout-cell layout-item-10" style="width: 15%" >
                <p style="text-align: center;">
                    @php
                        $class_a = "art-button";
                        $checked = '';
                        if(strtolower($item->var_data) == strtolower($variables->$s_var ?? null)){
                            $class_a = 'art-button chosen';
                            $checked = "checked";
                        }
                    @endphp
                    <a id="{{$item->id}}" class="{{$class_a}}" var_data="{{$item->var_data}}">{{$item->btn_text}}</a>
                    <input type="radio" name="choose-one" value="{{$item->id}}" class="hidden-input" {{$checked}} >
                </p>
            </div>
        </div>
    </div>
</div>
@endforeach
</div>

<script type="text/javascript">
    // var arr = JSON.parse($("#specify-values").val());
    var variables = JSON.parse($("#variables").val());
    if($("#specify-behavior").val() == 'substance_use_disorder')
        $('.columns_present').removeClass('specifiers-answers').addClass('substunce-specifiers-answers');
    $(document).off('click','.substunce-specifiers-answers a').on('click', ".substunce-specifiers-answers a", function(){
        id = $(this).attr('id');
        row = $( this ).closest(".substunce-specifiers-answers" );
        input = $(this).next("input");
        substance_id = $(".substance-class").attr("id");
        if($(this).hasClass("chosen")){
            $(this).removeClass('chosen')
            $(input).prop("checked", false);
            // delete arr["substance"][substance_id];
            // $("#specify-values").val(JSON.stringify(arr)); 
            if($("#variable").val() != ""){
                variables[$("#variable").val()] = '';
                $("#variables").val((JSON.stringify(variables)));
            }

        }
        else{
            $(row).find("a").removeClass("chosen");
            $(this).addClass("chosen");
            $(input).prop("checked", true);
            // if(jQuery.isEmptyObject(arr['substance']))
                // arr['substance'] = {};
                // arr['substance'][substance_id]=id;
            // if (jQuery.isEmptyObject(arr[group_name]))
            //     arr[group_name] = {};
            // arr["substance"][substance_id] = id;
            // $("#specify-values").val(JSON.stringify(arr));
            if($("#variable").val() != ""){
                variables[$("#variable").val()]=$(this).attr("var_data");
                $("#variables").val(JSON.stringify(variables));
            }           
        }
    })
</script>