<style>
.art-content .art-postcontent-0 .layout-item-5 { margin-top: 15px;margin-right: 20px;margin-bottom: 15px;margin-left: 20px;  }
.art-content .art-postcontent-0 .layout-item-6 { border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-width:0px;border-color:#E1E8EF; color: #111418; background: ;  border-collapse: separate; border-radius: 0px;  }
.art-content .art-postcontent-0 .layout-item-7 { color: #323B43; background: ; padding: 0px; vertical-align: bottom; border-radius: 0px;  }
.art-content .art-postcontent-0 .layout-item-8 { margin-top: 10px;margin-right: 20px;margin-bottom: 20px;margin-left: 20px;  }
.art-content .art-postcontent-0 .layout-item-9 { border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-width:0px;border-color:#E1E8EF; color: #111418; background: ; border-spacing: 27px 0px; border-collapse: separate; border-radius: 0px;  }
.art-content .art-postcontent-0 .layout-item-11 { border-style:Double;border-width:3px;border-color:#D8DEE4; color: #303841; background: #F7F7F8 url('/css/images/a64d4.png') scroll; padding: 5px; vertical-align: top; border-radius: 0px;  }
.ie7 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
.ie6 .art-post .art-layout-cell {border:none !important; padding:0 !important; }

</style>

<div class="art-content-layout-wrapper layout-item-5">
	<div class="art-content-layout layout-item-6">
		<div class="art-content-layout-row">
	    	<div class="art-layout-cell layout-item-7" style="width: 100%" >
				<p style="padding-left: 20px;">
					<span>
						<span class="f_ic1">
							<span style="font-size: 18px; font-style: italic; color: #2D3F53; -webkit-border-horizontal-spacing: 20px;">{{$specify->text}}
							</span>
						</span>
					</span><br>
				</p>
				<p></p>
			</div>
	    </div>
	</div>
</div>
<div class="specifiers-answers art-content-layout-wrapper layout-item-8">
@php
$s_var = $specify->variable;
if($specify->behavior && $specify->behavior->name == 'free_text_if_yes' && ($specify->variable == 'associated_medical' || $specify->variable == 'associated_another')){
	$variables->$s_var=($variables->$s_var->var_data ?? null);
}
$i=0;
$selected = $specify->variable;
@endphp
	@while(count($specifiers_items)>$i)
	@php
	$div_class = ($i>=4 ? 'art-content-layout layout-item-9 general-present-rows' : 'art-content-layout layout-item-9');
	@endphp
	<div class="{{$div_class}}">
		<div class="art-content-layout-row">
			@for($idx=0; $i < count($specifiers_items) && $idx<4; $idx++)
				<div class="art-layout-cell layout-item-11" style="width: 25%;text-align: center;" >
					<p>
						@php
							$item = $specifiers_items[$i];
							$i++;
							$class_a = "art-button";
							$checked = '';
							if(strtolower($item->var_data) == strtolower($variables->$s_var ?? null)){
								$class_a = 'art-button chosen';
								$checked = "checked";
							}
						@endphp
						<a id="{{$item->id}}" class="{{$class_a}}" var_data="{{$item->var_data}}">{{$item->btn_text}}</a>
						<input type="radio" name="choose-one" value="{{$item->id}}" class="hidden-input" {{$checked}} >
					</p>
					<p>
						<span style="font-size: 13px;">
						@if($specify->behavior && $specify->behavior->name == 'show_catatonia_criteria')
							<span class="diagnosis-name"><a href="/diagnosis/{{$breadcrumbs_str}}/specifiers/{{$specify->parent}}?return_to={{Request::path()}}">{{$item->text}}</a></span>
						@else
							{{$item->text}}
						@endif
						</span>
					</p>
				</div>
			@endfor
		</div>
	</div>
	@endwhile
</div>