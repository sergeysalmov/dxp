<style>
.art-content .art-postcontent-0 .rated-layout-item-4 { margin-top: 3px;  }
.art-content .art-postcontent-0 .rated-layout-item-5 { border-top-style:Solid;border-bottom-style:Solid;border-top-width:1px;border-bottom-width:1px;border-top-color:#4B5C6C;border-bottom-color:#4B5C6C;  }
.art-content .art-postcontent-0 .rated-layout-item-6 { border-top-style:Solid;border-right-style:Solid;border-left-style:Solid;border-top-width:1px;border-right-width:1px;border-left-width:1px;border-top-color:#5C7084;border-right-color:#5C7084;border-left-color:#5C7084; color: #0B0D0F; background: #F7F7F8 url('/css/images/5beff.png') scroll; padding: 5px;  }
.art-content .art-postcontent-0 .rated-layout-item-7 { border-top-style:Double;border-bottom-style:Double;border-top-width:0px;border-bottom-width:0px;border-top-color:#9FB4CB;border-bottom-color:#9FB4CB;border-top:none !important;  border-collapse: separate;  }
.art-content .art-postcontent-0 .rated-layout-item-8 { border-top-width:4px;border-top-style:Groove;border-top-color:#98AFC8;margin-top: 3px;margin-bottom: 3px;  }
.art-content .art-postcontent-0 .rated-layout-item-9 { border-right-style:Solid;border-bottom-style:Solid;border-left-style:Solid;border-right-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-color:#5C7084;border-bottom-color:#5C7084;border-left-color:#5C7084; color: #0B0D0F; background: ; padding: 5px;  }
.ie7 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
.ie6 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
</style>
@php
$items_group = Helper::group_specifiers_items($specifiers_items);
@endphp
<div class="clinitian_rated">
@foreach($items_group as $name=>$items)
    <div class="art-content-layout-wrapper rated-layout-item-4">
        <div class="art-content-layout rated-layout-item-5">
            <div class="art-content-layout-row">
            <div class="art-layout-cell rated-layout-item-6" style="width: 100%" >
                <p style="text-align: center;">
                <span style="line-height: 24px; -webkit-border-horizontal-spacing: 10px; -webkit-border-vertical-spacing: 10px; font-family: Verdana; font-weight: bold; font-size: 16px;">{{$name}}</span><br></p>
            </div>
            </div>
        </div>
    </div>
    <div id="{{$name}}" class="art-content-layout rated-layout-item-7 specifiers-rated">
        <div class="art-content-layout-row">
            @foreach($items as $key=>$item)
                <div class="art-layout-cell rated-layout-item-9" style="width: 20%" >
                    <p style="text-align: center;">
                        <span style="color: rgb(17, 20, 24); -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">
                            @php 
                                $class_a = "art-button";
                                $checked = '';
                            @endphp
                            <a id="{{$item->id}}" class="{{$class_a}}">{{$item->btn_text}}</a>
                            <input type="radio" name="rated-{{$name}}" value="{{$item->id}}" class="hidden-input" {{$checked}} >
                        </span>
                    </p>
                    <p>
                        <span style="-webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px; color: rgb(52, 64, 75); font-size: 13px;">{{$item->text}}</span><br>
                    </p>
                </div>
        @endforeach
        </div>
    </div>
    @if(!$loop->last)
        <div class="art-content-layout-br rated-layout-item-8"></div>
    @endif
@endforeach
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $(".art-content-layout-br.layout-item-3").addClass('clinitian-rated');
        $(".art-content-layout-br.bottom-divider").addClass('clinitian-rated');        
    })
    $(document).on('click', ".specifiers-rated a", function(){
        id = $(this).attr('id');
        row = $( this ).closest(".specifiers-rated" );
        input = $(this).next("input");
        group_name = $(this).closest(".specifiers-rated").attr("id");
        if($(this).hasClass("chosen")){
            $(this).removeClass('chosen')
            $(input).prop("checked", false);
            // delete arr["clinitian_rated"][group_name];
            // arr['clinitian_rated'][group_name] = {};
            // $("#specify-values").val(JSON.stringify(arr));    
        }
        else{
            $(row).find("a").removeClass("chosen");
            $(this).addClass("chosen");
            $(input).prop("checked", true);
            // if(jQuery.isEmptyObject(arr['clinitian_rated']))
                // arr['clinitian_rated'] = {}
            // if (jQuery.isEmptyObject(arr[group_name]))
            //     arr[group_name] = {};
            // arr["clinitian_rated"][group_name] = id;
            // $("#specify-values").val(JSON.stringify(arr));             
        }
        
    })
</script>