<style>
.art-content .art-postcontent-0 .layout-item-5 { margin-top: 15px;margin-right: 50px;margin-bottom: 15px;margin-left: 50px;  }
.art-content .art-postcontent-0 .layout-item-6 { border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-width:0px;border-top-color:#9FB4CB;border-right-color:#9FB4CB;border-bottom-color:#9FB4CB;border-left-color:#9FB4CB; color: #111418; border-spacing: 10px 0px; border-collapse: separate;  }
.art-content .art-postcontent-0 .layout-item-7 { border-top-width:2px;border-top-style:Solid;border-top-color:#98AFC8;margin-top: 10px;margin-right: 50px;margin-bottom: 10px;margin-left: 50px;  }
.art-content .art-postcontent-0 .layout-item-8 { color: #111418; background: ; padding-right: 10px;padding-left: 10px; vertical-align: middle;  }
.art-content .art-postcontent-0 .layout-item-9 { border-style:Double;border-width:3px;border-color:#D8DEE4; color: #0B0D0F; background: #F7F7F8 url('/css/images/cd5a5.png') scroll; padding: 0px; vertical-align: middle; border-radius: 5px;  }
.art-content .art-postcontent-0 .layout-item-10 { border-style:Double;border-width:3px;border-color:#D8DEE4; color: #0B0D0F; background: #F7F7F8 url('/css/images/cbd73.png') scroll; padding: 0px; vertical-align: middle; border-radius: 5px;  }
.art-content .art-postcontent-0 .layout-item-11 { border-style:Double;border-width:3px;border-color:#D8DEE4; color: #0B0D0F; background: #F7F7F8 url('/css/images/a745a.png') scroll; padding: 0px; vertical-align: middle; border-radius: 5px;  }
.art-content .art-postcontent-0 .layout-item-12 { border-style:Double;border-width:3px;border-color:#D8DEE4; color: #0B0D0F; background: #F7F7F8 url('/css/images/6ba3d.png') scroll; padding: 0px; vertical-align: middle; border-radius: 5px;  }
.art-content .art-postcontent-0 .layout-item-13 { border-style:Double;border-width:3px;border-color:#D8DEE4; color: #0B0D0F; background: #F7F7F8 url('/css/images/e0504.png') scroll; padding: 0px; vertical-align: middle; border-radius: 5px;  }
.art-content .art-postcontent-0 .layout-item-14 { border-style:Double;border-width:3px;border-color:#D8DEE4; color: #0B0D0F; background: #F7F7F8 url('/css/images/29c29.png') scroll; padding: 0px; vertical-align: middle; border-radius: 5px;  }
.art-content .art-postcontent-0 .layout-item-16 { color: #111418; background: ; padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px; vertical-align: top; border-radius: 5px;  }
.art-content .art-postcontent-0 .layout-item-17 { border-style:Double;border-width:3px;border-color:#D8DEE4; color: #0B0D0F; background: #F7F7F8 url('/css/images/1e62c.png') scroll; padding: 0px; vertical-align: middle; border-radius: 5px;  }
.ie7 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
.ie6 .art-post .art-layout-cell {border:none !important; padding:0 !important; }

</style>
@php
$items_group = Helper::group_specifiers_items($specifiers_items);
$selected = $specify->variable;
@endphp
<div class="specifiers-answers">
    @foreach($items_group as $key=>$items)
        @if(!$loop->first)
            <div class="art-content-layout-br layout-item-7"></div>
        @endif
        <div class="art-content-layout-wrapper layout-item-5">
            <div class="art-content-layout layout-item-6">
                <div class="art-content-layout-row">
                    <div class="art-layout-cell layout-item-8" style="width: 50%" >
                        <p style="text-align: center;">
                            <span style="color: rgb(43, 61, 80); font-size: 18px; text-align: left; -webkit-border-horizontal-spacing: 5px;">{{$key}}</span><br>
                        </p>
                    </div>
                    <!-- <div class="lines-specifiers-buttons" style="width: 50%"> -->
                    @foreach($items as $k=>$item)
                        @if(count($items)==1)
                        <div class="art-layout-cell layout-item-16" style="width: 25%" >
                            <p style="text-align: center;">
                                <span style="-webkit-border-horizontal-spacing: 5px;"><br></span>
                            </p>
                        </div>
                        @endif
                        <div class="art-layout-cell layout-item-11" style="width: 25%%" >
                            <p style="text-align: center;">
                                <span style="-webkit-border-horizontal-spacing: 5px;">
                                    @php 
                                        $class_a = "art-button";
                                        $checked = '';
                                        if($item->var_data == ($variables->$selected ?? null)){
                                            $class_a = 'art-button chosen';
                                            $checked = "checked";
                                        }
                                    @endphp
                                    <a id="{{$item->id}}" class="{{$class_a}}" var_data="{{$item->var_data}}">{{$item->btn_text}}</a>
                                    <input type="radio" name="choose-one" value="{{$item->id}}" class="hidden-input" {{$checked}} >
                                </span><br>
                            </p>
                        </div>
                    @endforeach
                    <!-- </div> -->
                </div>
            </div>
        </div>
    @endforeach
</div>