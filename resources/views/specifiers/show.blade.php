@extends(( $layout ? 'layouts.app' : 'layouts.ajax' ))
@section('title', 'Specifiers')
@section('content')

<style>
.art-content .art-postcontent-0 .layout-item-0 { margin-bottom: 3px;  }
.art-content .art-postcontent-0 .layout-item-old-1 { border-spacing: 10px 0px; border-collapse: separate;  }
.art-content .art-postcontent-0 .layout-item-1 { margin-top: 3px;margin-bottom: 7px;  }
.art-content .art-postcontent-0 .layout-item-2 { border-spacing: 20px 0px; border-collapse: separate;  }
.art-content .art-postcontent-0 .layout-item-3 { border-top-width:2px;border-top-style:Groove;border-top-color:#7493B4;margin-right: 20px;margin-left: 20px;  }
.art-content .art-postcontent-0 .layout-item-4 { padding-right: 7px;padding-left: 7px;  }

.art-content .art-postcontent-0 .layout-item-old-0 { margin-bottom: 10px;  }

.ie7 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
.ie6 .art-post .art-layout-cell {border:none !important; padding:0 !important; }

</style>
<input type="hidden" id="next-specify" value="{{$next_specify}}">
<input type="hidden" id="diagnosis_id" value="{{$breadcrumbs_str}}">
<input type="hidden" id="behavior" value="{{$behavior}}">
<input type="hidden" id="specify-behavior" value="{{$specify->behavior_id  ? App\Behavior::find($specify->behavior_id)->name : false}}">
<input type="hidden" id="is_require" value="{{$specify->is_require}}">
<input type="hidden" id="return_to" value="{{$return_to}}">
<input type="hidden" id="specify_id" value="{{$specify->id}}">
<input type="hidden" id="variables" value="{{$variables == '{}' ? '{}' : json_encode($variables)}}">
<input type="hidden" id="variable" value="{{$specify->variable}}">

<div class="art-sheet clearfix">
    <div class="art-layout-wrapper">
    	<div class="art-content-layout">
        	<div class="art-content-layout-row">
                <div class="art-layout-cell art-content">
                	<article class="art-post art-article">
                		<div class="art-postcontent art-postcontent-0 clearfix">
                			<div class="art-content-layout-wrapper layout-item-0">
								<div class="art-content-layout layout-item-old-1">
    								<div class="art-content-layout-row">
    									<div class="art-layout-cell top-page" style="width: 100%" >
										    <p class = "breadcrumbs">
										        <span>{!! Helper::get_breadcrumbs($breadcrumbs) !!}</span>
										    </p>
										    <p class = diagnosis-name-top>
                                                @if(!Helper::criteria_specifiers($specify))
										          <span>{{$diagnosis->name}}</span>
                                                @endif
										    </p>
    									</div>
								    </div>
								</div>
							</div>
							<div class="art-content-layout-wrapper layout-item-1">
								<div class="art-content-layout layout-item-2">
								    <div class="art-content-layout-row">
								    	<div class="art-layout-cell layout-item-4" style="width: 100%" >
        									<h2>
        										<span class="specify-name">{{$specify->title}}:</span>
        										@if(!empty($specify->explanation))
        											<span class="specify-explanation">({{$specify->explanation}})</span>
        										@endif
        									</h2>
    									</div>
    								</div>
								</div>
							</div>
							<div class="art-content-layout-br layout-item-3"></div>
							@include('specifiers._'. $present_type->name, ['specify' => $specify, 'specifiers_items' => $specifiers_items, 'diagnosis' => $diagnosis,'variables' =>$variables,'breadcrumbs_str' => $breadcrumbs_str])
                            <div class="art-content-layout-br bottom-divider"></div>
                            @include('general._menu', ['page' => 'specifiers','note' =>$specify->note, 'info' => $specify->info])
                            <div class="art-content-layout-wrapper layout-item-old-0">
                                <div class="art-content-layout layout-continue-div">
                                    <div class="art-content-layout-row">
                                        <div class="art-layout-cell continue-div" style="width: 100%" >
                                            <p style="text-align: center;">
                                                <a id ="specify-continue-btn" class="art-button">Continue</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
 						</div>
					</article>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection