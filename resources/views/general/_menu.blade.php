@php
$options_src ="/css/images/settings-cogwheel-button%20(2)-2.png";
$note_src = ($note ? "/css/images/edit-button%20(2)-3-2-2-2-2-2-2.png" : "/css/images/note-g.png");
$info_src = ($info ? "/css/images/Note32-3-2-2-2-2-2-2.png" : "/css/images/more-info-g.png");
$present = App\User::get_present_favorite();
$class_a = $page == 'criteria' ? 'criteria active-menu' : 'gray-menu'; 
@endphp
<div class="art-content-layout layout-menu-buttons">
    <div class="art-content-layout-row">
         <div class="art-layout-cell menu-buttons" style="width: 100%" >
            <p>
                <div class="dropdown">
                    <div class="options tooltip with_content">
                        <img class="dropbtn" width="20" height="20" alt="Options" src="{{$options_src}}" style="margin-top: 6px; margin-left: 3px;">
                        <span class="tooltiptext">Options</span>
                    </div>
                    <div class="dropdown-content">
                        <a href="#" id="one" class="{{$class_a}}">
                            <span>Show Criterion</span>
                            <input type="checkbox" name="present_type" value="one" class="criterion menu-icon" {{($present == 'one' ? "checked" : '')}} disabled>
                        </a>
                        <a href="#" id="all" class="{{$class_a}}">
                            <span>Show Criteria</span>
                            <input type="checkbox" name="present_type" value="all" class="criteria menu-icon" {{($present == 'all' ? "checked" : '')}} disabled>
                        </a>
                        <a href="/sigma" class="active-menu">
                            <span>Sigma</span>
                            <span class="menu-icon sigma-icon">&rarr;</span>
                        </a>
                    </div>
                </div>
            <div class="note tooltip {{$note ? 'with_content' : 'empty'}}">
                <img width="25" height="25" alt="Note" src="{{$note_src}}" class="" style="margin-top: 6px; margin-left: 10px;">
                <span class="tooltiptext">Note</span>
            </div>
            <div class="info tooltip {{$info ? 'with_content' : 'empty'}}">
                <img width="25" height="25" alt="More Information" src="{{$info_src}}" class="" style="margin-top: 3px; margin-right: 5px; margin-left: 10px;">
                <span class="tooltiptext">Learn More</span>
            </div>
<!--                 <img width="20" height="20" alt="Options" src="/css/images/settings-cogwheel-button.png" style="margin-top: 6px; margin-left: 3px;">&nbsp; &nbsp; &nbsp;
                <img width="25" height="25" alt="" class="art-lightbox" src="/css/images/note-g.png">&nbsp; &nbsp;&nbsp;
                <img width="25" height="25" alt="" class="art-lightbox" src="/css/images/more-info-g.png"> -->
            </p>
        </div>
    </div>
</div>
<div id="info-popup">
    <div class="info-note-popup">
        <div class ="close-btn x-close"></div>
        <div class="content">{!! nl2br(e($info))  !!}</div>
        <div class="close-bottom x-close"><p><a class="art-button">Close</a></p></div>
    </div> 
</div>
<div id="note-popup">
    <div class="info-note-popup">
        <div class ="close-btn x-close"></div>
        <div class = "content">{!! nl2br(e($note)) !!}</div>
        <div class="close-bottom x-close"><p><a class="art-button">Close</a></p></div>
    </div>
</div>
