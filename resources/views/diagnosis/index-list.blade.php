@extends(( $layout ? 'layouts.app' : 'layouts.ajax' ))
@section('title', 'Classification cluster')
@section('content')
<link rel="stylesheet" href={{ URL::asset("css/diagnos-list.css")}} media="all">

<div class="art-sheet clearfix">
            <div class="art-layout-wrapper">
                <div class="art-content-layout">
                    <div class="art-content-layout-row">
                        <div class="art-layout-cell art-content"><div class="art-vmenublock clearfix">
        <div class="art-vmenublockcontent">
<ul class="art-vmenu">
  @foreach ($diagnosis as $key => $value)
  <li><a href="/diagnosis/{{$key}}">{{$value['name']}}</a></li>
  @endforeach
</ul>

</div>
</div>
<article class="art-post">
  <div class="art-postcontent art-postcontent-0 clearfix">
    <div class="art-content-layout layout-item-0">
    <div class="art-content-layout-row">
    <div class="art-layout-cell" style="width: 100%" >
        <p><span style="-webkit-border-vertical-spacing: 10px;"><a href="../diagnosis_view?view=matrix">
          <img width="31" height="23" alt="" src="css/images/tmp22A1.png" style="float: left; margin-top: 0px;">
        </a>
            <span style="font-size: 12px;">&nbsp;Switch to matrix view</span>
          </span>
          <br>
        </p>
    </div>
    </div>
    </div>
</article>
</div>
</div>
</div>
</div>
</div>

@endsection
