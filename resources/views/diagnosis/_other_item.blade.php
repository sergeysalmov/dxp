<style>.art-content .art-postcontent-0 .layout-item-0 { margin-top: 0px;margin-bottom: 10px;  }
.art-content .art-postcontent-0 .layout-item-4 { color: #111418; border-spacing: 25px 0px; border-collapse: separate;  }
.art-content .art-postcontent-0 .layout-item-5 { border-style:Dotted;border-top-width:1px;border-right-width:1px;border-bottom-width:1px;border-left-width:1px;border-top-color:#9FB4CB;border-right-color:#9FB4CB;border-bottom-color:#9FB4CB;border-left-color:#9FB4CB; color: #0B0D0F; background: #EEF2F6;background: rgba(238, 242, 246, 0.6); padding-top: 10px;padding-right: 20px;padding-bottom: 10px;padding-left: 10px; vertical-align: top; border-radius: 5px;  }
.ie7 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
.ie6 .art-post .art-layout-cell {border:none !important; padding:0 !important; }

</style>

<div class="art-content-layout-wrapper layout-item-0">
    <div class="art-content-layout layout-item-4">
        <div class="art-content-layout-row">
        @foreach($children as $key => $item)
            @if(!empty($item['children']))

                <div class="diagnosis-name art-layout-cell layout-item-5" style="width: 50%" >
                    <p class="MsoNormal" style="text-align: center;"><span style="font-weight: bold; color: #34404B;font-size:14px;">{{$item['name']}}</span></p>
             
                    <ul>
                    @foreach($item['children'] as $k => $v)
                        <li><a href="/diagnosis/{{$level}}.{{$key}}.{{$k}}" style=" font-weight: bold;"><span style="color: #000000;font-size:14px;">{{$v['name']}}</span></a><br></li>
                    @endforeach
                    </ul>
                </div>
            @else
                @php($unspecified= array('key' =>$key, 'item' => $item))
            @endif
        @endforeach

        </div>
    </div>
</div>
@if(!empty($unspecified))
<div class="art-content-layout-wrapper layout-item-0">
    <div class="art-content-layout layout-item-4">
        <div class="art-content-layout-row">
            <div class="diagnosis-name art-layout-cell layout-item-5" style="width: 50%" >
                <ul>
                    <li><a href="/diagnosis/{{$level}}.{{$unspecified['key']}}" style=" font-weight: bold;font-size:14px;"><span style="color: #000000;">{{$unspecified['item']['name']}}</span></a><br></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endif




