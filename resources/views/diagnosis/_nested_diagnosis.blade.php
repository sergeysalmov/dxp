<div class="art-content-layout-wrapper layout-item-4">
	<div class="art-content-layout layout-item-5">
    	<div class="art-content-layout-row">
    		<div class="diagnosis-name art-layout-cell layout-item-6" style="width: 100%" >
                @if(count($breadcrumbs) == 1 && ($has_children || !empty($ncd)))
                    <p style="padding-left: 20px;">
                		<span style="font-family: Tahoma;"><b>
                            <a href= "/diagnosis/{{$level.'.'.$key}}">
                                <span   id = "{{$key}}" class ="disorder_list" level ="{{$level.'.'.$key}}" style="font-size: 14px; color: rgb(0, 0, 0);">{{$name}}
                                </span>
                            </a></b>
                        </span>
            		</p>

            		{!! Helper::renderMenu($children,$level . '.' . $key,$type) !!}
                @else
                    <b>{!! Helper::renderMenu($children,$key) !!}</b>
                @endif
    		</div>
   		</div>
	</div>
</div>