<style>.art-content .art-postcontent-0 .other-layout-item-0 { margin-top: 0px;margin-bottom: 10px;  }
.art-content .art-postcontent-0 .other-layout-item-2 { border-top-width:1px;border-top-style:Solid;border-top-color:#9FB4CB;margin-top: 10px;margin-bottom: 10px;  }
.art-content .art-postcontent-0 .other-layout-item-4 { margin-top: 10px;margin-bottom: 0px;  }
.art-content .art-postcontent-0 .other-layout-item-5 { color: #111418; border-spacing: 25px 0px; border-collapse: separate;  }
.art-content .art-postcontent-0 .other-layout-item-6 { border-top-style:Dotted;border-right-style:Dotted;border-left-style:Dotted;border-top-width:1px;border-right-width:1px;border-left-width:1px;border-top-color:#9FB4CB;border-right-color:#9FB4CB;border-left-color:#9FB4CB; color: #0B0D0F; background: #EEF2F6;background: rgba(238, 242, 246, 0.6); padding-top: 0px;padding-right: 10px;padding-bottom: 0px;padding-left: 10px; vertical-align: middle; border-radius: 5px;  }
.art-content .art-postcontent-0 .other-layout-item-7 { border-right-style:Dotted;border-bottom-style:Dotted;border-left-style:Dotted;border-right-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-color:#9FB4CB;border-bottom-color:#9FB4CB;border-left-color:#9FB4CB; color: #0B0D0F; background: #EEF2F6;background: rgba(238, 242, 246, 0.6); padding-top: 0px;padding-right: 20px;padding-bottom: 10px;padding-left: 10px; vertical-align: top; border-radius: 5px;  }
.art-content .art-postcontent-0 .other-layout-item-12 { margin-top: 10px;margin-bottom: 10px;  }
.art-content .art-postcontent-0 .other-layout-item-13 { border-top-style:Dotted;border-bottom-style:Dotted;border-top-width:1px;border-bottom-width:1px;border-top-color:#9FB4CB;border-bottom-color:#9FB4CB; color: #0B0D0F; background: #EEF2F6;background: rgba(238, 242, 246, 0.6); padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px; vertical-align: top; border-radius: 5px;  }
.ie7 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
.ie6 .art-post .art-layout-cell {border:none !important; padding:0 !important; }

</style>
@foreach($children as $key =>  $item)
    @php
    $columns_present = Helper::other_columns_present($item['children']);
    @endphp
    @if($columns_present)
        <div class="art-content-layout-wrapper other-layout-item-4">
            <div class="art-content-layout other-layout-item-5">
                <div class="art-content-layout-row">
                    <div class="diagnosis-name art-layout-cell other-layout-item-6" style="width: 100%" >
                        <p class="MsoNormal" style="text-align: center;"><a href="/diagnosis/{{$level}}.{{$key}}"><span style="font-weight: bold; color: #A83800; font-size:14px;">{{$item['name']}}</span></a>        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="art-content-layout-wrapper other-layout-item-0">
            <div class="art-content-layout other-layout-item-5">
                <div class="art-content-layout-row">
                    @foreach($item['children'] as $k=>$v)
                        @if(!empty($v['children']))
                            <div class="diagnosis-name art-layout-cell other-layout-item-7" style="width: 50%" >
                                <p class="MsoNormal" style="text-align: center;"><span style="font-weight: bold; color: #34404B;font-size:14px;">{{$v['name']}}</span></p>
                                    <ul>
                                        @foreach($v['children'] as $k1=>$v1)
                                        <li><a href="/diagnosis/{{$level}}.{{$key}}.{{$k}}.{{$k1}}"><span style="color: rgb(0, 0, 0);font-size:14px;">{{$v1['name']}}</span></a><br>
                                        </li>
                                        @endforeach
                                    </ul><p>
                                </p>

                            </div>
                        @else
                            @php($unspecified=array('key'=>$k,'val'=>$v))
                        @endif
                    @endforeach
                </div>
            </div>
            @if(!empty($unspecified))
                <div class="art-content-layout other-layout-item-5">
                    <div class="art-content-layout-row">
                        <div class="diagnosis-name art-layout-cell other-layout-item-7" style="width: 50%" >
                            <ul>
                                <li><a href="/diagnosis/{{$level}}.{{$key}}.{{$unspecified['key']}}"><span style="color: rgb(0, 0, 0);font-size:14px;">{{$unspecified['val']['name']}}</span></a><br>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            @endif    
        </div>
    @else
        <div class="art-content-layout-wrapper other-layout-item-12">
            <div class="art-content-layout other-layout-item-5">
                <div class="art-content-layout-row">
                    <div class="diagnosis-name art-layout-cell other-layout-item-13" style="width: 100%" >
                        <p style="padding-left: 20px;"><a href="/diagnosis/{{$level}}.{{$key}}"><span style="font-weight: bold; color: #A83800;font-size:14px;">{{$item['name']}}</span></a><br>
                        </p>
                        <ul>
                        @foreach($item['children'] as $k => $v)
                            <li><a href="/diagnosis/{{$level}}.{{$key}}.{{$k}}"><span style="color: rgb(0, 0, 0);font-size:14px;">{{$v['name']}}</span></a><br></li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(!$loop->last)
        <div class="art-content-layout-br other-layout-item-2"></div>
    @endif
@endforeach