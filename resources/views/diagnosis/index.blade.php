@extends(( $layout ? 'layouts.app' : 'layouts.ajax' ))
@section('title', 'Classification cluster')
@section('content')
<link rel="stylesheet" href={{ URL::asset("css/diagnos.css")}} media="all">
<div class="art-sheet clearfix">
    <div class="art-layout-wrapper art-content-layout art-content-layout-row art-layout-cell art-content">
        <article class="art-post art-article">
            <div class="art-postcontent art-postcontent-0 clearfix">
                      @php
                      $diagnosis_chunk = array_chunk($diagnosis,4);
                      @endphp
                      @foreach ( $diagnosis_chunk as $diagnosis_array)
                      <div class="art-postcontent art-postcontent-0 clearfix art-content-layout-wrapper layout-item-0 art-content-layout layout-item-1">
                              <div class="art-content-layout-row responsive-layout-row-2">
                          @foreach ($diagnosis_array as $key => $diagnos)
                          <div class="art-layout-cell layout-item-2" style="width: 25%; line-height: 22px; text-align: center;">
                              <p>
                                  <a href="/diagnosis/{{$diagnos['id']}}" title="{{$diagnos['name']}}">
                                    <span style="font-family: Tahoma; font-weight: bold; color: rgb(40, 57, 75); ">{{$diagnos['name']}}</span>
                                  </a>
                                <br>
                              </p>
                          </div>
                          @endforeach
                          </div>
                          </div>
                      @endforeach
                    </div>

                    <div class="art-content-layout layout-item-3">
                        <div class="art-content-layout-row responsive-layout-row-1">
                        <div class="art-layout-cell layout-item-4" style="width: 100%">
                            <p><a href="/diagnosis_view?view=list">
                              <img alt="" src="css/images/list.png" style="float: left; margin-top: 0px; margin-left: 3px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px;" width="31" height="31"></a>
                              <span style="font-size: 12px; color: #000000;">&nbsp;Switch to clusters list view</span></p>
                        </div>
                        </div>
                    </div>
                    </div>
        </article>
    </div>
</div>

<div id="art-resp"><div id="art-resp-m"></div><div id="art-resp-t"></div></div><div id="art-resp-tablet-landscape"></div><div id="art-resp-tablet-portrait"></div><div id="art-resp-phone-landscape"></div><div id="art-resp-phone-portrait"></div>
@endsection
