<link rel="stylesheet" href={{ URL::asset("css/list/bootstrap.min.css")}} media="all">
<link rel="stylesheet" href={{ URL::asset("css/list/bootstrap-table.min.css")}} media="all">
<link rel="stylesheet" href={{ URL::asset("css/list/custom.css")}} media="all">
@extends('layouts.app')
@section('content')
<body>
    <div class="art-sheet clearfix">
    <table id="table-list"
      data-toggle="table"
      data-search="true"
      data-url="api/list"
      data-sort-name="name"
      data-sort-order="asc"
      data-height="630"
      data-pagination="true"
      data-page-size="25"
      data-page-list="[25, 50, 100, 200, All]"
      >
      <thead>
        <tr>
          <th data-field="name" data-sortable="true">Name</th>
          <th data-field="ICD10">ICD 10</th>
        </tr>
      </thead>
    </table>
  </div>
  <script src={{ URL::asset("js/list/jquery-3.3.1.min.js")}} ></script>
  <script src={{ URL::asset("js/list/popper.min.js")}}></script>
  <script src={{ URL::asset("js/list/bootstrap.min.js")}}></script>
  <script src={{ URL::asset("js/list/bootstrap-table.min.js")}}></script>
  <script>
  $('#table-list').bootstrapTable({
    onClickRow: function (row, $element, field) {
      window.location.href="diagnosis/"+row.id+"/criteria";
    }
  })
  </script>
</body>
@stop
