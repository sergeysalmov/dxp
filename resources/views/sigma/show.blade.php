@extends(( $layout ? 'layouts.app' : 'layouts.ajax' ))
@section('content')
<div class="art-sheet clearfix">
    <div class="art-layout-wrapper">
    	<div class="art-content-layout">
        	<div class="art-content-layout-row">
                <div class="art-layout-cell art-content">
                	<article class="art-post art-article">
                		<div class="art-postcontent art-postcontent-0 clearfix">
							<h1>Sigma page</h1>
							<p>Formal -</p>
							<div class="formals-separate"></div>
							<!-- <table> -->
							@foreach($formals as $key => $value)
							<!-- <tr> -->
								<!-- <p class="formal-item"> -->
									@if(!empty($value['multi_icd']))
									<table class= "formals multi_icd">
										@foreach($value['multi_icd'] as $k=>$v)
										<tr>
											<td class="code">{!! $v['code'] !!}</td>
											<td>{!! $v['text'].($loop->last ? '' : ';')!!}</td>
										</tr>
										@endforeach
										@if(!empty($value['text']))
											<tr>
												<td class="code"></td>
												<td>{!! $value['text'] !!}</td>
											</tr>
										@endif
									</table>
									@else
									<table class= "formals">
										@foreach($value as $k=>$v)
										@php
										$code_style = '';//(strstr($v['code'], '[') ? 'color:grey;' : '');
										$text_style = '';(strstr($v['text'], '[') ? 'color:grey;' : '');
										@endphp
										<tr>
											<td class="code" style="{{$code_style}}">{!! $v['code'] !!}</td>
											<td style="{{$text_style}}">{!! $v['text'] !!}</td>
										</tr>
										@endforeach
									</table>
									@endif
								<!-- </p> -->
							<!-- </tr> -->
							<div class="formals-separate"></div>
							@endforeach
							<!-- </table> -->
							@if(count($history)>0)
								<ul class="history">
								<p>Diagnosis History</p>
								@foreach($history as $key => $val)
									<li>
										<a href = "{{$val->url}}">{{$val->title}}</a>
										<span>{{$val->created_at}}</span>
									</li>
								@endforeach
								</ul>
							@endif
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
<!-- Trigger/Open The Modal -->
