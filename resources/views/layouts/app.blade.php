<html dir="ltr" class="firefox firefox71 desktop custom-responsive" lang="en-US"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Diagnostic Program</title>
    <meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width">
    <link rel="stylesheet" href={{ URL::asset("css/main.css")}} media="all">
    <link rel="stylesheet" href={{ URL::asset("css/myStyle.css")}} media="all">
    <link rel="stylesheet" href={{ URL::asset("css/main.responsive.css")}} media="all">
    
    <script src={{ URL::asset("js/jquery.js")}}></script>
    <script src={{ URL::asset("js/main.js")}}></script>
    <script type="text/javascript" src={{ URL::asset("js/diagnosis.js")}} ></script>
    <script src={{ URL::asset("js/script.js")}}></script>
    <script src={{ URL::asset("js/script.responsive.js")}}></script>
    <script src={{ URL::asset("js/main.responsive.js")}}></script>
</head>

<body>
<div id="art-main">
<header class="art-header" style="background-position: 944.78px 43.55px, 11.22px 3.23px, 0px 0px;">
    <div class="art-shapes">
    </div>
    <h1 class="art-headline" style="left: 349px; top: 33.6px;; margin-left: 0px !important;">
    <a href="/">Diagnostic Program</a>
    </h1>
    <h2 class="art-slogan" style="left: 408px; top: 9.75px;; margin-left: 0px !important;">Virtual Psychology</h2>
</header>

<nav class="art-nav desktop-nav">
    <ul class="art-hmenu"><li ><a href="/">Home</a></li>
    <li><a href="/diagnosis">Clusters</a></li>
    <li><a href="/list">List</a></li>
    <li><a href="/checker">Checker</a></li></ul>
</nav>
<div class="container">
    @yield('content')
</div>
<footer class="art-footer">
  <div class="art-footer-inner">
<div class="art-content-layout-wrapper layout-item-0">
<div class="art-content-layout layout-item-1">
    <div class="art-content-layout-row responsive-layout-row-1">
    <div class="art-layout-cell layout-item-2" style="width: 100%">
        <p><span style="color: rgb(47, 57, 68); font-family: Verdana;">Home
 &nbsp;| &nbsp;Diagnostic Center &nbsp;| &nbsp;Records Cabinet &nbsp;|
&nbsp;Documentation &nbsp;| &nbsp;Account &nbsp;| &nbsp;Logout</span><br></p><p><br><span style="font-size: 11px;"><span style="font-family: Verdana; color: #3E4C59;">DxP Version 5.19&nbsp;</span><span style="font-family: Verdana; color: #3E4C59;">© Virtual Psychology</span></span><span style="color: rgb(47, 57, 68); font-family: Verdana;"><br></span></p>
    </div>
    </div>
</div>
</div>

</div>
</footer>

</div>

    <div id="validation-error">
        <div class="modal-content">
            <div class="modal-header">
                <span class="close">&times;</span>
                <span>Validation Error</span>
            </div>
            <div class="modal-body">
                <p>You must fill all criteria</p>
            </div>
            <div class="modal-footer">
                <span class ="close">close</span>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal"></div>
<div id="art-resp"><div id="art-resp-m"></div><div id="art-resp-t"></div></div><div id="art-resp-tablet-landscape"></div><div id="art-resp-tablet-portrait"></div><div id="art-resp-phone-landscape"></div><div id="art-resp-phone-portrait"></div></body></html>
