@extends( 'layouts.app')

@section('content')
<div class="art-sheet clearfix">
    <div class="art-layout-wrapper">
        <div class="art-content-layout">
            <div class="art-content-layout-row">
                <div class="art-layout-cell art-content">
                    <map name="menu_map" id="menu_map">
                    <area shape="rect" coords="337,204,564,259" href="" alt="Symptom Checker">
                    <area shape="circle" coords="268,239,20" href="/">
                    <area shape="circle" coords="292,147,20" href="/list">
                    <area shape="rect" coords="335,119,562,174" href="/list">
                    <area shape="rect" coords="333,27,560,82" href="{{route('diagnosis.index')}}">
                    <area shape="circle" coords="271,60,20" href="{{route('diagnosis.index')}}">
                    </map>
                    <img src="<?=asset('images/home-dock.png')?>" alt="" style="border-color: initial; margin-top: 2px; margin-left: 7px; border-width: 0px; width: 100%; max-width: 570px; height: auto;" usemap="#menu_map" width="570" height="293">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection