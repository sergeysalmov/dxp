(function($){
// var dialogModule = modulejs.require("dialog");


// $(document).ready(function(){
window.history.replaceState({prevUrl: 'first'}, document.title, document.location.href);

$(window).bind('popstate',function(event){
  event.preventDefault();
  var state = event.originalEvent ? event.originalEvent.state : event.state;
  if(state && window.location){
    window.location.reload();
    
  }
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

display_disorders = function(){

    $(document).on('click',".diagnosis-name a, .history a",function(e){
        e.preventDefault();
        url = $(this).attr('href');
        $.ajax({
            type: "GET",
            url: url,
            success: function (data, textStatus, request) {
                successFunc(data,request);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });             
    })

    $(document).on('click', ".criterion-answers a", function(){
        input = $(this).next("input")
        if($(this).hasClass('chosen')){
            $(this).removeClass('chosen')
            $(input).prop('checked',false)
        }
        else{
            row = $( this ).closest( ".criterion-answers" );
            $(row).find("a").removeClass("chosen");
            $(this).addClass("chosen");
            $(input).prop("checked", true);
        }
    })
    $(document).on('click', '.criterion-answers label.art-radiobutton', function(){
        input = $(this).next("input")
        if($(this).hasClass('art-checked')){
            $(this).removeClass('art-checked')
            $(input).prop('checked',false)
        }
        else{
            row = $( this ).closest( ".criterion-answers" );
            $(row).find("label.art-radiobutton").removeClass("art-checked");
            $(this).addClass("art-checked");
            $(input).prop("checked", true);
        }


    });
    var unanswered = [];
    $(document).on('click','#continue-btn', function(){
        var all_answered = true;
        $(".criterion-answers input[type=radio]").each(function(){
            var name = $(this).attr("name");
            if($("input:radio[name="+name+"]:checked").length == 0){
                all_answered = false;
                unanswered.push(name.replace('criterion-',''));
            }
        });
        if(all_answered){

            other = $("#other-popup");
            if(other.length)
            {
                if($("input:radio:checked").val() == "Yes"){
                    other = $(other).find(".other-popup-content");
                    $(".popuptext").removeClass('show');
                    openModalPopup(other);
                    lostFocus();
                    return;
                }
            }
            save_criteria();
        }
        else{
            validation = $("#validation-error").find(".modal-content");
            $(validation).find("p").text("Please fill all criteria.");
            openModalPopup(validation);
        }

    })
    $(document).on('click', '#other-enter-btn',function(){
        free_text = $("#myModal .other-text-area").val();
        if(free_text ==''){
            popup = $(".popuptext");
            popup.addClass("show"); setTimeout(function () {
                $(popup).removeClass('show');
            }, 30000);
            return
        }
        save_criteria(free_text);
        closeModalPopup();

    })

    $(document).on('click','#specify-continue-btn', function(){

        // arr = (window.location.origin + window.location.pathname).split('/');
        specify_id = $("#specify_id").val();
        // values = $("#specify-values").val();
        next_specify = $("#next-specify").val();
        diagnosis_id = $("#diagnosis_id").val();
        behavior = $("#behavior").val();
        specify_behavior = $("#specify-behavior").val();
        is_require = $("#is_require").val();
        return_to = $("#return_to").val();
        variables = $("#variables").val();
        variable = $("#variable").val();
        var_obj = $.parseJSON(variables);
        if(specify_behavior == 'free_text_if_yes')
            variables = remove_empty(var_obj,variable);
        data ={'diagnosis_id': diagnosis_id, 'specify_id': specify_id, 'next_specify': next_specify,'behavior':behavior,'return_to': return_to,'variables':variables}     
        if(specify_behavior == 'least_one_of_group'){
            s_data = var_obj[variable];
            if(!s_data || s_data.length == 0){
                least_one_chosen(specify_id,function(myCallback){
                    if(!myCallback){
                        validation = $("#validation-error").find(".modal-content");
                        $(validation).find("p").text("You must choose at least one option from the specifiers group of impairements in reading, writing, or mathematics.");
                        openModalPopup(validation);
                    }
                    else{
                        save_specifiers(data)
                    }
                });
                return;
            }
        }
        else{
            if(is_require == "1"){
                if(!var_obj || !var_obj[variable]){
                    validation = $("#validation-error").find(".modal-content");
                    $(validation).find("p").text("This is a required specifier. To continue, please provide an input.");
                    openModalPopup(validation);
                    return;

                }   
            }
        }
        save_specifiers(data) 
    });
  
    $(document).on('click', 'span.close,a.close', function(){
        closeModalPopup();
    })
    $(document).on('click', function(event) {
        if (event.target == $("#myModal")[0] && $("#myModal").css('display') == 'block') {
            closeModalPopup();
        }
        else{
            if(event.target != $(".options")[0] && event.target != $('img.dropbtn')[0] && $(".menu-buttons .dropdown .dropdown-content").css('display') == 'block'){
                $(".menu-buttons .dropdown .dropdown-content").css('display', 'none');
            }            
        } 
    });
    $(document).on('click', ".specifiers-answers a", function(e){
        e.preventDefault();
        id = $(this).attr('id');
        row = $( this ).closest(".specifiers-answers" );
        input = $(this).next("input");
        var variables = JSON.parse($("#variables").val());
        var variable = $("#variable").val();
        var behavior =$("#specify-behavior").val();
        if(behavior == 'ncd_types')
            ncd_url(this);
        else{    
            if($(input).attr('type') == 'radio'){
                if($(this).hasClass("chosen")){
                    $(this).removeClass('chosen')
                    $(input).prop("checked", false);
                    // $("#specify-values").val('');
                    if(variable != ""){
                        variables[variable] = '';
                        $("#variables").val((JSON.stringify(variables)));
                    }
        
                }
                else{
                    $(row).find("a").removeClass("chosen");
                    $(this).addClass("chosen");
                    $(input).prop("checked", true);
                    // $("#specify-values").val('{"item_id": "'+id+'"}');             
                    if(variable != ""){
                        if(behavior=="free_text_if_yes" && (variable=="associated_medical" || variable == "associated_another"))
                            variables[variable]= {"var_data": $(this).attr("var_data")};
                        else
                            variables[variable]=$(this).attr("var_data");
                        $("#variables").val(JSON.stringify(variables));
                    }           
                }
            }
            else if($(input).attr('type') == 'checkbox'){
                var_data = $(this).attr('var_data');
                // values = $('#specify-values').val();
                // $values = variables[variable];
                // jsonobj = $.parseJSON(values);
                // if(!jsonobj['item_ids'])
                //     jsonobj['item_ids'] = []
                if(!variables[variable])
                    variables[variable] = [];
                if($(this).hasClass("chosen")){
                    $(this).removeClass('chosen')
                    $(input).attr('checked',false)
                    variables[variable].splice($.inArray(var_data, variables[variable]),1);
                }
                else{
                    $(this).addClass("chosen");
                    $(input).prop("checked", true);
                    variables[variable].push(var_data);
                }
                $("#variables").val(JSON.stringify(variables))
            }
        }

    })
    $(document).on('click', '.info.with_content', function(){
        info = $("#info-popup").find('.info-note-popup');
        info_content = $(info).find(".content");
        if($(info_content).text().match("^popup-"))
            loadPage($.trim($(info_content).text()));
        else
            openModalPopup(info);

    });

    $(document).on('click', '.note.with_content', function(){
        note = $("#note-popup").find('.info-note-popup');
        note_content = $(note).find(".content");
        if($(note_content).text().match("^popup-"))
            loadPage($.trim($(note_content).text()));
        else
            openModalPopup(note);
    });

    $(document).on('click','.options.with_content', function(){
        $(".menu-buttons .dropdown .dropdown-content").css('display','block');
    });

    $(document).on('click','.menu-buttons .dropdown-content a', function(){

    })
    $(document).on('click','.dropdown-content a.criteria.active-menu',function(e){
        e.preventDefault();
        input = $(this).find($('input[name="present_type"]:checked'));
        if(input.length == 0)
        $('input[name="present_type"]').prop('checked',false);
        $(input).prop('checked',true);
        present_favorite = $(this).attr('id');
        diagnosis_id = $("#diagnosis_id").val();
        user_id = $("#user_id").val();
        $.ajax({
            type: "PUT",
            url: '/users/' + user_id,
            data: {'id': user_id, 'present_favorite': present_favorite, 'diagnosis_id': diagnosis_id},
            success: function (data, textStatus, request) {
                window.location.reload();
                // successFunc(data,request)
           },
            error: function (data) {
                console.log('Error:', data);
            }
        });         
    })

    $(document).on('click','#choose_patient_btn',function(e){
        e.preventDefault();
        patient_id = $("#patient_input").val();
        if(patient_id !=''){
            $.ajax({
                type: "GET",
                url: '/patient_diagnosis/create_patient_session',
                // dataType: 'json',
                data: {'patient_id': patient_id},
                success: function (data, textStatus, request) {
                    window.location.reload();
               },
                error: function (data) {
                }
            });
        }         
    })

    $(document).on('click','.x-close', function(){
        closeModalPopup();
    })

    $(document).on('click', ".popup-art-main a", function(e){
        e.preventDefault();
        if($(this).hasClass('popup-chosen')){
            $(this).removeClass('popup-chosen')
        }
        else{
            popup = $( this ).closest( ".popup-art-main" );
            $(popup).find("a").removeClass("popup-chosen");
            $(this).addClass("popup-chosen");
            a_text = $(this).text();
            a = $(".specifiers-answers a:contains('"+a_text+"')");
            // if(!$(a).hasClass('chosen')){
                $(a).trigger('click');
                closeModalPopup();
            // }


        }
    })

    $(document).on('click', '.popuptext .close-popup', function(){
        $(".popuptext").removeClass('show');
    })

    $(document).on('change','input[name="present_type"]', function(){
        // $('input[name="present_type"]').prop('checked',false);
        // $(this).prop('checked',true);
        // present_favorite = $(this).val();
        // diagnosis_id =$("#diagnosis_id").val();
        // $.ajax({
        //     type: "PUT",
        //     url: '/users/1',
        //     data: {'id': 1, 'present_favorite': present_favorite, 'diagnosis_id': diagnosis_id},
        //     success: function (data, textStatus, request) {
        //         window.location.reload();
        //         // successFunc(data,request)
        //    },
        //     error: function (data) {
        //         console.log('Error:', data);
        //     }
        // });          
    })
    function successFunc(data,request){
        $(".container").html(data);
        url = request.getResponseHeader("urlPath");
        if(url.indexOf('return_from_catatonia') >= 0 || url.indexOf('return_from_ncd') >= 0){
            arr = url.split('?');
            url = arr[0];
        }
        document.title = request.getResponseHeader("title");
        window.history.pushState({prevUrl:window.location.href}, document.title, url);
    }

    errorFunc = function(data)
    {
     $(".container").html('<h1>500 Internal Server Error</h1>');   
        // window.history.pushState({prevUrl:window.loction.href},document.title,url)
    }


    loadPage = function(page_name){
        page = page_name + '.html'
        $("#myModal").load('/' + page, function(){
            $("#myModal").show();
            $("body").addClass("modal-open");
            load_popup_selections();
        });
    }

    load_popup_selections = function(){
        a = $(".specifiers-answers a.chosen")[0];
        if(a){
            a_text = $(a).text();
            console.log( a_text);
            popup_a = $("#myModal a:contains('"+a_text+"')")[0];
            $(popup_a).addClass("chosen");
        }
       // $(popup_a).trigger('click');
    }

    save_criteria = function(free_text){
        free_text = (typeof free_text === 'undefined') ? '' : free_text;
        var answers = {};
        $(".criterion-answers").each(function(){
            id = $(this).attr('id');
            radio = $("#" + id +" input:radio:checked")
            answers[id] = radio.val();
        });
        patient_diagnosis_id = $("#patient_diagnosis_id").val();
        next_criterion = $("#next-criterion").val();
        criterion_page = $("#criterion-page").val();
        diagnosis_id = $("#diagnosis_id").val();
        return_to = $("#return_to").val();
        return_type = $("#return_type").val();
        nested_return_to = $("#nested_return_to").val();
        $.ajax({
            type: "POST",
            url: '/diagnosisSteps',
            data: {'diagnosis_id': diagnosis_id, 'patient_id': patient_diagnosis_id, 'answers': answers, 'free_text': free_text, 'next_criterion': next_criterion, 'criterion_page': criterion_page, 'return_to': return_to, 'return_type': return_type, 'nested_return_to': nested_return_to},
            //dataType:'json',
            success: function (data, textStatus, request) {
                successFunc(data,request);

           },
            error: function (data) {
                console.log('Error:', data);
                // errorFunc(data);
            }
        }); 
    }

    save_specifiers = function(data){
        $.ajax({
            type: "POST",
            url: '/specifiers/store',
            data: data,
            success: function (data, textStatus, request) {
                successFunc(data,request)
           },
            error: function (data) {
                console.log('Error:', data);
            }
        });    
    }
    lostFocus = function(){
        $("textarea.other-input").blur();
    } 

    least_one_chosen = function(specify_id, cb){
        var result;
        $.ajax({
            type: "GET",
            url: '/specifiers/least_one_chosen',
            data: {'specify_id': specify_id},
            success: function (data) {
                result = data.one_chosen;
                cb(data.one_chosen)
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }

    ncd_url = function(btn_link){
        url = $(btn_link).attr('href');
        $.ajax({
            type: "GET",
            url: url,
            success: function (data, textStatus, request) {
                successFunc(data,request);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });   
    }

    remove_empty = function(obj,variable){
        new_obj=obj;
        if(obj[variable]){
            $.each( obj[variable], function( key, value ) {
                if(key.match("^free-text") && blank(value['code']) && blank(value['name']))
                    delete new_obj[variable][key];
            });
            console.log(new_obj);
        }
        return JSON.stringify(new_obj);
    }
    
    blank = function(variable){
        if(variable == "" || typeof(variable) =='undefined')
            return true;
        return false;
    }

    openModalPopup = function(popup){
        $("#myModal").html(popup.clone());
        $("#myModal").show();
        $("body").addClass("modal-open"); 
    }

    closeModalPopup = function(){
        $("#myModal").html('');
        $("#myModal").hide();
        $("body").removeClass("modal-open");
        if(unanswered.length>0) {
            for(x in unanswered) {
                $("#criterion-panel-"+unanswered[x]).addClass("marked");
            }
            unanswered = [];
        }
    }

}
display_disorders();

})(jQuery);
