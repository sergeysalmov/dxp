<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
   
    protected $table = 'diagnosis_history';

    protected $fillable = ['patient_diagnosis_id','title','url'];


}
