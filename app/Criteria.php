<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;


class Criteria extends Model
{
   
    protected $table = 'criteria';

    // protected static function boot()
    // {
    //     parent::boot();

    //     static::addGlobalScope('crit_as_title', function (Builder $builder) {
    //         $builder->where('behavior', '<>', 'crit_as_title');
    //     });
    // }

    public function diagnosis()
    {
        return $this->belongsTo('App\Diagnosis');
    }

    public static function get_note($id)
    {
    	$criteria = self::find($id);
    	return $criteria->note;
    }
    public static function get_info($id)
    {
    	$criteria = self::find($id);
    	return $criteria->info;
    }
    public static function show_letter($diagnosis)
    {
        $criteria = DB::select('select * from '.DB::getTablePrefix().'criteria where diagnosis_id='.$diagnosis->id.' and parent=0 or parent is NULL');
        return (count($criteria) > 1 ? true : false);
    }


}
