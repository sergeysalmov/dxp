<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientDiagnosisVariables extends Model
{
   
    protected $table = 'patient_diagnosis_variables';

}