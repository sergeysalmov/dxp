<?php 
namespace App\Http\Middleware;
use Illuminate\Support\Facades\Log;

use Closure;


// If Laravel >= 5.2 then delete 'use' and 'implements' of deprecated Middleware interface.
class AddHeaders
{
    public function handle($request, Closure $next)
    {
       $response = $next($request);
         $response->header('urlPath', $request->fullUrl());
        $response->header('title', $request->title);

        return $response;
    }
}