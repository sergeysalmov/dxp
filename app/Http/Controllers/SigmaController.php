<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Criteria;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Diagnosis;
use App\DiagnosisSteps;
use App\PatientDiagnosis;
use App\History;
use App\SpecifiersItems;
use App\PatientDiagnosisVariables;
use App\Specify;
use Illuminate\Support\Facades\DB;


class SigmaController extends Controller
{

    public function index(Request $request)
    {
        $patient_diagnosis_id = $request->session()->get('patient');
        $capitalize_words = $this->capitalize_words();
        $res = DB::select('select c.diagnosis_id,c.id criterion,ds.result from '.DB::getTablePrefix().'criteria c left join '.DB::getTablePrefix().'diagnosis_steps ds on ds.patient_diagnosis_id ='.$patient_diagnosis_id.' and ds.criterion_id = c.id where c.diagnosis_id in(select distinct diagnosis_id from '.DB::getTablePrefix().'diagnosis_steps where patient_diagnosis_id='.$patient_diagnosis_id.' and (status <> "related" or status is null)) and c.behavior not in( "crit_as_title","prob_pos_crit") order by ds.updated_at desc');
        $diagnosis_arr=array();
        $formals =array();
        foreach ($res as $key => $value) {
            if(empty($diagnosis_arr[$value->diagnosis_id]))
                $diagnosis_arr[$value->diagnosis_id] = array('full' => true);
            if(empty($value->result))
                $diagnosis_arr[$value->diagnosis_id]['full'] = false;
        }
        foreach ($diagnosis_arr as $key => $value) {
            if($value['full']){
                $diagnosis = Diagnosis::find($key);
                if(strpos($diagnosis->behavior, 'major_ncd') === false && strpos($diagnosis->behavior, 'mild_ncd') === false){
                    $formals[$key] = $this->get_formal($diagnosis,$patient_diagnosis_id,$capitalize_words);
                }

            }
        }
        $history  = History::where('patient_diagnosis_id',$patient_diagnosis_id)->orderBy('created_at', 'desc')->get();
        $request->title = 'Sigma';
        History::create(['patient_diagnosis_id' => $request->session()->get('patient'), 'title' => 'Sigma','url' => $request->fullUrl()]);
        return view('sigma.show',['layout' => !$request->ajax(),'history' => $history, 'formals' =>$formals]);
    }


    public function get_formal($diagnosis,$patient_diagnosis_id,$cwords)
    {
        $code = $diagnosis->ICD10;
        $formal = array();
        $text = '';
        $associated = '';
        $and = '';
        $multiple_icd = array();
        $multiple_icd_codes = false;
        $variables = array();
        $criteria = DB::select('select c.*,ds.result,ds.free_text from '.DB::getTablePrefix().'criteria c left join '.DB::getTablePrefix().'diagnosis_steps ds on ds.patient_diagnosis_id='.$patient_diagnosis_id. ' and c.id=ds.criterion_id where c.diagnosis_id='.$diagnosis->id.' and c.parent=0 and c.behavior <> "crit_as_title" and (ds.status is null or ds.status <> "related")');
        // return formal if not all criteria = yes
        foreach ($criteria as $key => $value) {
            if(!in_array($value->result, array("Yes","Probable","Possible"))){
                $formal[0] = array('code' => '','text' => 'Full criteria for '.$diagnosis->name.' have not been met');
                return $formal;
            }
        }
        // return formal if disorder is 'Other'
        if (count($criteria) == 1 && $criteria[0]->behavior == 'free_text') {
            $text = $criteria[0]->free_text;
            // $formal[0] = array('code' => $code, 'text' => $diagnosis->name.', '.$text);
            // return $formal;
        }

        $specifiers = $diagnosis->specifiers;
        if(!empty($specifiers) && count($specifiers)>0)
        {
            $variables = $this->get_patient_diagnosis_variables($diagnosis,$patient_diagnosis_id);
            $specifiers = DB::select('select s.id,s.diagnosis_id,s.title,s.text,s.groups,s.is_require,s.variable,s.parent,s.additional_text,b.name as behavior_name from '.DB::getTablePrefix().'specifiers s left join '.DB::getTablePrefix().'behaviors b on s.behavior_id=b.id  where s.diagnosis_id='.$diagnosis->id);
            foreach ($specifiers as $key => $specifier) {
                // $var = $specifier->variable;
                if($specifier->parent)
                    $specifier = $this->get_parent($specifier);
                $var = $specifier->variable;
                $selections = $variables->$var ?? null;
                if($specifier->is_require && empty($selections) && $this->require_severity($specifier,$variables) && $this->require_alcohol_type($specifier,$variables) && $this->require_delirium($specifier,$variables,$diagnosis) && $this->require_bipolar($specifier,$variables,$diagnosis) && $this->require_bipolar2($specifier,$variables,$diagnosis) && $this->require_major_depressive($specifier,$variables,$diagnosis)   || ($specifier->behavior_name == 'least_one_of_group' && !$this->is_select_from_group($specifier,$specifiers,$variables))){
                    $formal[0] = array('code' => '','text' => 'Full criteria for '.$diagnosis->name.' have been met. Still, some of the diagnosis required specifiers are missing.');
                    return $formal;
                }
                else{
                    // $result = json_decode($specifier->result);
                    $var_items = NULL;
                    if(!empty($selections)){
                        if(gettype($selections) == 'string')
                            $var_items = addslashes($selections);
                        elseif (gettype($selections) == 'array')
                            $var_items = join("','",$selections);
                    // elseif (!empty($result->clinitian_rated)) {
                    //     $ids_arr=array();
                    //     foreach ($result->clinitian_rated as $k => $v) {
                    //         array_push($ids_arr, $v);
                    //         $ids .= join("','",$ids_arr);
                    //     }
                    // }
                       
                            
                    }
                    // $ids = ($result->item_id ?? (!empty($result->item_ids) ? join("','",$result->item_ids) : NULL));
                    $specifiers_items = DB::select("select * from ".DB::getTablePrefix()."specifiers_items where specify_id=".$specifier->id." and var_data in('$var_items') order by specify_id");
                    $count_items = count($specifiers_items);
                    if($count_items && $specifier->groups)
                        $multiple_icd[$specifier->id]= array();
                    switch ($specifier->behavior_name) {
                        case 'free_text_if_yes':
                        case 'with_disorder_of':
                            if($diagnosis->behavior == 'another_medical'){
                                // $medicals = $this->get_another_medical_formal($selections);
                                // array_push($formal,$medicals);
                            }
                            elseif($diagnosis->behavior == 'associated_input' && !in_array($diagnosis->id,[13,22,251])){
                                list($associated_formal,$associated_text) = $this->get_free_text_associated($specifier,$variables);
                                $text .= ' '.$associated_text;
                                // if(!empty($selections)) {
                                //     foreach ($selections as $k1 => $v1) {
                                //         if(strstr($k1, 'free-text-')){
                                //             $associated .= $and.($v1->name ?? null);
                                //             $and = ' and with ';
                                //         }
                                //     }
                                // }
                            }
                            else{
                                if(!empty($selections)) {
                                    foreach ($selections as $k1 => $v1) {
                                        if(strstr($k1, 'free-text-')){
                                            $associated .= $and.($v1->name ?? null);
                                            $and = ' and with ';
                                        }
                                    }
                                }

                            }
                            break;
                        case 'standart_input':
                        case 'behavioral_input':
                            $text .= ' '.$selections;
                            break;
                        case 'psycho_stressor':
                            if(!empty($variables->with_psy_stressor) && $variables->with_psy_stressor =='with')
                                $text .= ' '.$selections;
                            break;
                        case 'show_catatonia_criteria':
                        case 'catatonia':
                            if($selections=="Yes")
                                $catatonia = true;
                                // $catatonia = array('code' => 'F06.1', 'text' => 'catatonia associated with '.strtolower($diagnosis->name));
                            break;
                            case 'bi_with_catatonia':
                            $specify = Specify::find($specifier->id);
                                 if($specify->catatonia_result($diagnosis->id) == 'Yes')
                                    $catatonia = true;
                                break;
                        case 'with_use_severity':
                            if(!$this->require_severity($specifier,$patient_diagnosis_id,$variables)){
                                break;
                            }
                        case 'substance_name':
                        case 'substance_use_disorder':
                        case 'with_use_severity':
                        case 'delirium':
                        case 'small_substance_name':
                        case 'prescribed_medication':
                            break;
                        default:
                            // if($count_items > 1){
                            foreach ($specifiers_items as $k => $v) {
                                if($specifier->groups){ //specific learning disorder
                                    $multiple_icd[$specifier->id]['code'] = $v->icd;
                                    if(!empty($multiple_icd[$specifier->id]['text']))
                                        $multiple_icd[$specifier->id]['text'] .= ' and ';
                                    else
                                        $multiple_icd[$specifier->id]['text']='';
                                    $multiple_icd[$specifier->id]['text'] .= $v->icd_text;
                                    $multiple_icd_codes = true;
                                }
                                elseif(!empty($v->icd_text)){
                                    if(!empty($v->icd))
                                        $code = $v->icd; 
                                    $text .= ', '.$v->icd_text;  
                                }

                            }
                            if(!empty($multiple_icd[$specifier->id])){
                                $multiple_icd[$specifier->id]['s_text'] = strtolower($specifier->text);
                            }
                        break;
                    }                        

                }

            }
        }
        if($multiple_icd_codes){
            $formal= array();
            $formal['text'] = $text;
            foreach ($multiple_icd as $k => $v) {
                $m_text =  strtr(strtolower($diagnosis->name.' '. ($v['s_text'] ?? '') . ', '.$v['text']),$cwords);
                $arr = array('code' => $v['code'],'text' => $m_text);
                if(empty( $formal['multi_icd']))
                    $formal['multi_icd'] = array();
                array_push($formal['multi_icd'], $arr);
            }
        }
        else
        {
            $associated  = (empty($associated) ? '' : ' associated with '.$associated);
            if($diagnosis->behavior != 'adult_abuse')
                $formal = array(0 => array('code' => $code,'text' => strtr(strtolower($diagnosis->name.$associated.$text),$cwords)));
            else
                $formal = array(0 => array('code' => $code,'text' => strtr(strtolower(str_replace(', ', '',$text)),$cwords)));
            // array_push($formal,array('code' => $code,'text' => strtolower($diagnosis->name.$associated.$text)));
            if(strpos($diagnosis->behavior, 'substance_use') !== false)
                $formal[0] = $this->get_substance_formal($diagnosis,$patient_diagnosis_id,$variables,$text,$cwords);
            elseif($diagnosis->behavior == 'delirium')
                $formal[0] = $this->get_delirium_formal($diagnosis,$patient_diagnosis_id,$variables,$text,$cwords);
            elseif (strpos($diagnosis->behavior, 'mild_type') !==false) 
                $formal[0] = $this->get_mild_ncd_formal($diagnosis,$patient_diagnosis_id,$variables,$text,$cwords);
            elseif (strpos($diagnosis->behavior, 'major_type') !==false) 
                $formal[0] = $this->get_major_ncd_formal($diagnosis,$patient_diagnosis_id,$variables,$text,$cwords);
            // elseif(strpos($diagnosis->behavior,'bipolar') !== false)
            elseif($diagnosis->behavior == 'bipolar')
                $formal[0] = $this->get_bipolar_formal($diagnosis,$patient_diagnosis_id,$variables,$text,$cwords);
            elseif($diagnosis->behavior == 'major_depressive')
                $formal[0] = $this->get_major_depressive_formal($diagnosis,$patient_diagnosis_id,$variables,$text,$cwords);
            if (strpos($diagnosis->behavior, 'another_medical') !==false)
                $formal = $this->get_another_medical_formal($formal,$diagnosis,$patient_diagnosis_id,$variables,$text,$cwords);
            if(!empty($associated_formal))
                $formal = array_merge($associated_formal,$formal);
            // elseif (strpos($diagnosis->behavior, 'associated_input') !== false)
            //     $formal = $this->get_associated_formal($formal,$diagnosis,$patient_diagnosis_id,$variables,$text);
            if(!empty($catatonia)){
                // $formal[0]['text'] .= $this->get_catatonia_formal($diagnosis);
                //$formal[1] = $catatonia;
                $catatonia_formal = $this->get_catatonia_formal($diagnosis,$cwords);
                // array_push($formal, $catatonia);
                array_push($formal, $catatonia_formal);

            }  
        }
        return  $formal;
    }


    private function is_select_from_group($specifier,$specifiers,$selections)
    {
        $s_var = $specifier->variable;
        foreach($specifiers as $k=>$v){
            if(!empty($v->groups) && $v->groups == $specifier->groups && !empty($selections)){
                $s_var = $v->variable;
                if(!empty($selections->$s_var))
                    return true;
            }
        }
        return false;
    }

    private function get_bipolar_formal($diagnosis,$patient_diagnosis_id,$variables,$icd_text,$cwords)
    {
        $code ='';
        $column='mild';
        $episode = str_replace(['_current','_recent'], '', $variables->episode_type);
        $variables->psychotic = isset($variables->psychotic) ? $variables->psychotic : ''; 
        $variables->severity = isset($variables->severity) ? $variables->severity : '';
        $variables->remission = isset($variables->remission) ? $variables->remission: '';
        if(in_array($variables->episode_type, ['manic_current','depressive_current']))
            $column = ($variables->psychotic=='yes' ? 'with_psychotic' : $variables->severity);
        elseif($variables->episode_type=='unspecified')
            $column = 'unspecified';
        elseif(in_array($variables->episode_type, ['manic_recent','hypomanic_current','hypomanic_recent','depressed_recent'])){
            $column = $variables->remission;
        }
        $res = DB::select('select * from '.DB::getTablePrefix().'mood_codes where episode="'.$episode.'"');
        if(!empty($res)){
            $res = $res[0];
            $code=$res->$column;
        }
        $formal = array('code'=>$code, 'text'=>strtolower($diagnosis->name.$icd_text));
        return $formal;
    }

    private function get_major_depressive_formal($diagnosis,$patient_diagnosis_id,$variables,$icd_text,$cwords)
    {
        $code ='';
        $column='mild';
        $episode = $variables->single_recurrent;
        $variables->psychotic = isset($variables->psychotic) ? $variables->psychotic : ''; 
        $variables->severity = isset($variables->severity) ? $variables->severity : '';
        $variables->remission = isset($variables->remission) ? $variables->remission: '';
        if($variables->current=='yes')
            $column = ($variables->psychotic=='yes' ? 'with_psychotic' : $variables->severity);
        elseif($variables->current=='no')
            $column = $variables->remission;
        $res = DB::select('select * from '.DB::getTablePrefix().'mood_codes where episode="'.$episode.'"');
        if(!empty($res)){
            $res = $res[0];
            $code=$res->$column;
        }
        $formal = array('code'=>$code, 'text'=>strtolower($diagnosis->name.$icd_text));
        return $formal;
    }

    private function get_substance_formal($diagnosis,$patient_diagnosis_id,$variables,$icd_text,$cwords)
    {
        $variables->substance_class = (!empty($variables->substance_class) ? $variables->substance_class : '');
        $variables->use_disorder = (!empty($variables->use_disorder) ? strtolower($variables->use_disorder) : '');
        $variables->severity = (!empty($variables->severity) ? strtolower($variables->severity) : null);
        $code='';
        $name = ($variables->substance_class ? ucfirst($variables->substance_class) : '');
        if(!empty($variables->use_disorder) && $variables->use_disorder == 'with' && $variables->severity)
            $severity = ($variables->severity == 'mild' ? 'mild' : ($variables->severity=='moderate' || $variables->severity=='severe' ? 'moderate_or_severe' : ''));
        else
            $severity = 'without';
        $n_name = (!empty($variables->alcohol_type) ? $variables->alcohol_type : $name);
        $res = DB::select('select * from '.DB::getTablePrefix().'substance_medication_induced where substance="'.$n_name.'" and diagnosis_id='.$diagnosis->id);
        if(!empty($res))
            $code = $res[0]->$severity;
        $substance_name = (!empty($variables->substance_input) ? $variables->substance_input : $name);
        if($diagnosis->behavior == 'delirium')
            $disorder_name = strtolower($substance_name.' intoxication delirium');
        else
            $disorder_name = strtolower(str_replace('Substance/Medication',$substance_name,$diagnosis->name));
        if($variables->use_disorder == "with")
            $text = $variables->severity. ' '.$substance_name.' use disorder with '.$disorder_name;
        else
            $text = $disorder_name;
        $formal = array("code"=>$code,"text"=>$text.$icd_text);
        return  $formal;
   }

    private function get_delirium_formal($diagnosis,$patient_diagnosis_id,$variables,$text,$cwords)
    {
        $formal = array("code"=>"000","text"=>"Delirium in developing");
        switch ($variables->delirium_whether) {                
            case 'substance intoxication delirium':
                $formal = $this->get_substance_formal($diagnosis,$patient_diagnosis_id,$variables,$text);
                break;
            case 'substance withdrawal delirium':
                $formal = $this->get_withdrawal_delirium_formal($diagnosis,$patient_diagnosis_id,$variables,$text);
                break;
            case 'medication-induced delirium':
                $formal = $this->get_medication_induced_delirium_formal($diagnosis,$patient_diagnosis_id,$variables,$text);
                break;
            case 'delirium due to another medical condition':
                $formal = $this->get_delirium_due_another_formal($diagnosis,$patient_diagnosis_id,$variables,$text);
                break;
            case 'delirium due to multiple etiologies':
                $formal = $this->get_delirium_due_multiple_formal($diagnosis,$patient_diagnosis_id,$variables,$text);
                break;
            default:
                break;

        }
        return $formal;

    }

    private function get_withdrawal_delirium_formal($diagnosis,$patient_diagnosis_id,$variables,$icd_text)
    {
        $variables->substance_class = (!empty($variables->substance_class) ? $variables->substance_class : '');
        $variables->use_disorder = (!empty($variables->use_disorder) ? strtolower($variables->use_disorder) : '');
        $variables->severity = (!empty($variables->severity) ? strtolower($variables->severity) : null);
        $code='';
        $name = ($variables->substance_class ? ucfirst($variables->substance_class) : '');
        $res = DB::select("select si.id,si.icd from ".DB::getTablePrefix()."specifiers_items si inner join ".DB::getTablePrefix()."specifiers s on s.id=si.specify_id left join ".DB::getTablePrefix()."behaviors b on b.id=s.behavior_id where s.diagnosis_id=".$diagnosis->id." and b.name='small_substance_name' and var_data='".$variables->substance_class."'");
        if(!empty($res))
            $code = $res[0]->icd;
        $substance_name = (!empty($variables->substance_input) ? $variables->substance_input : $name);
        $disorder_name = strtolower($substance_name.' withdrawal delirium');
        if($variables->use_disorder == "with")
            $text = $variables->severity. ' '.$substance_name.' use disorder with '.$disorder_name;
        else
            $text = $disorder_name;
        $formal = array("code" => $code, "text" => $text.$icd_text);
        return $formal;
    }

    private function get_medication_induced_delirium_formal($diagnosis,$patient_diagnosis_id,$variables,$icd_text)
    {
        $code ='';
        $medication_class = (!empty($variables->medication_class) ? $variables->medication_class : '');
        $medication = (!empty($variables->substance_input) ? $variables->substance_input : $medication_class);
        $res = DB::select("select si.id,si.icd from ".DB::getTablePrefix()."specifiers_items si inner join ".DB::getTablePrefix()."specifiers s on  s.id=si.specify_id left join ".DB::getTablePrefix()."behaviors b on b.id=s.behavior_id where s.diagnosis_id=".$diagnosis->id." and b.name='prescribed_medication' and si.var_data='".$medication_class."'");
        if(!empty($res))
            $code = $res[0]->icd;
        $formal = array("code" => $code, "text" => $medication."-induced delirium".$icd_text);
        return $formal;

    }

    private function get_delirium_due_another_formal($diagnosis,$patient_diagnosis_id,$variables,$icd_text)
    {
        $text='';
        $code='';
        $free_text="free-text-1";
        if(!empty($variables->other_medical && !empty($variables->other_medical->$free_text))){
            $code = (!empty($variables->other_medical->$free_text->code) ? $variables->other_medical->$free_text->code : '');
            $name = (!empty($variables->other_medical->$free_text->name) ? $variables->other_medical->$free_text->name : '');
        }
        $res = DB::select('select icd from '.DB::getTablePrefix().'specifiers_items si inner join '.DB::getTablePrefix().'specifiers s on s.id=si.specify_id where s.diagnosis_id='.$diagnosis->id.' and var_data="'.$variables->delirium_whether.'"');
        if(!empty($res))
            $code2=$res[0]->icd;
        if($code && $name)
            $formal = array("code"=>$code,"text"=>$name."; ".$code2." delirium due to ".$name.$icd_text);
        else
            $formal = array("code"=>$code2,"text"=> "delirium due to unspecified medical condition" );
        return $formal;
    }

    private function get_delirium_due_multiple_formal($diagnosis,$patient_diagnosis_id,$variables,$icd_text)
    {
        $text='';
        $code='';
        $multiple = (!empty($variables->multiple) ? $variables->multiple.'; ' : '');
        $res = DB::select('select icd from '.DB::getTablePrefix().'specifiers_items si inner join '.DB::getTablePrefix().'specifiers s on s.id=si.specify_id where s.diagnosis_id='.$diagnosis->id.' and var_data="'.$variables->delirium_whether.'"');
        if(!empty($res))
            $code=$res[0]->icd;
        if($multiple){
            $text = $multiple.$code.' delirium due to multiple etiologies'.$icd_text;
            $code = '';
        }
        else{
            $text = 'delirium due to multiple etiologies'.$icd_text;
        }
        $formal = array("code"=>$code,"text"=> $text );
        return $formal;        
    }

    private function get_mild_ncd_formal($diagnosis,$patient_diagnosis_id,$variables,$icd_text,$cwords)
    {
        $formal = array("code"=>"","text" =>"FALSE/".$diagnosis->name);
        $code = $diagnosis->ICD10;
        $mild_ncd_diagnosis = Diagnosis::find($diagnosis->parent);
        $var_data = trim(str_replace('Mild', '', $diagnosis->name));
        $res = DB::select('select s.id,s.parent from '.DB::getTablePrefix().'specifiers s left join '.DB::getTablePrefix().'behaviors b on s.behavior_id=b.id where diagnosis_id='.$mild_ncd_diagnosis->id.' and b.name="ncd_types"');
        if(!empty($res)){
            if(!empty($res[0]->parent))
                $specify_id = $res[0]->parent;
            else
                $specify_id = $res[0]->id;
            $res2 = DB::select('select icd_text from '.DB::getTablePrefix().'specifiers_items where specify_id='.$specify_id.' and LOWER(var_data)="'.strtolower($var_data).'"');
            if(!empty($res2)){
                $type_name = $res2[0]->icd_text;
                $text = strtr(strtolower($mild_ncd_diagnosis->name.' '.$type_name.' '.$icd_text),$cwords);
                $formal = array("code" => $code, "text" => $text);
            }
        }
        return $formal;
    }

    private function get_major_ncd_formal($diagnosis,$patient_diagnosis_id,$variables,$icd_text,$cwords)
    {
        $formal = array("code"=>"","text" =>"FALSE/".$diagnosis->name);
        $type_name = '';
        $major_ncd_diagnosis = Diagnosis::find($diagnosis->parent);
        $var_data = trim(str_replace('Major', '', $diagnosis->name));
        $res = DB::select('select * from '.DB::getTablePrefix().'diagnosis_steps where diagnosis_id='.$diagnosis->id.' and result in("Probable","Possible")');
        if(!empty($res))
            $kind = $res[0]->result;
        else
            $kind = "";
        $behavioral = (($kind == 'Possible' || empty($variables->behavioral)) ? '' : $variables->behavioral);
        $major_name = $major_ncd_diagnosis->name;
        $res = DB::select('select s.id,s.parent from '.DB::getTablePrefix().'specifiers s left join '.DB::getTablePrefix().'behaviors b on s.behavior_id=b.id where diagnosis_id='.$major_ncd_diagnosis->id.' and b.name="ncd_types"');
        if(!empty($res)){
            $specify_id = (!empty($res[0]->parent) ? $res[0]->parent : $res[0]->id);
            $res = DB::select('select icd_text from '.DB::getTablePrefix().'specifiers_items where specify_id='.$specify_id.' and LOWER(var_data)="'.strtolower($var_data).'"');
            if(!empty($res))
                $type_name = $res[0]->icd_text;            
        }
        $res = DB::select('select * from '.DB::getTablePrefix().'ncd_codes where diagnosis_id='.$diagnosis->id.' and behavioral="'.$behavioral.'" and probable_possible ="'.strtolower($kind).'"');
        if(!empty($res)){
            if($res[0]->med_code){
                $code = $res[0]->med_code;
                $text = $res[0]->med_name.'; '.$res[0]->icd_code.' '.strtolower($kind).' '.strtolower($major_name).' '.$type_name.$icd_text;
            }
            else{
                $code = $res[0]->icd_code;
                $text = strtr(strtolower($kind).' '.strtolower($major_name).' '.$type_name.$icd_text,$cwords);
            }
            $formal = array('code' => $code, "text" => $text);
        }
        return $formal;
    }
    private function get_another_medical_formal($formal,$diagnosis,$patient_diagnosis_id,$variables,$text,$cwords)
    {
        $code_holder = '[ICD code]';
        $name_holder = '[Name of the medical condition]';
        $medical_text='';
        $medicals = array();
        $another_medical = (empty($variables->another_medical) ? '' : $variables->another_medical);
        if(empty($another_medical) || empty((array) $another_medical)){
            array_push($medicals, array('code' => $code_holder, 'text' => $name_holder.';'));
            $medical_text = $name_holder;
        }
        else{
            $len = count((array) $another_medical);
            foreach($another_medical as $k=>$v){
                $code = (empty($v->code) ? $code_holder : $v->code);
                $name = (empty($v->name) ? $name_holder : $v->name);
                if(!empty($medical_text))
                    $medical_text.=', ';
                if($len > 1 && !next($another_medical))
                    $medical_text .='and ';
                $medical_text .= $name;
                array_push($medicals,array('code'=>$code,'text'=>$name.';'));
            }
        }
        $formal[0]['text'] = strtolower(str_replace('another medical condition',$medical_text,$formal[0]['text']));
        $formal = array_merge($medicals,$formal);
        return $formal;
    }

    private function get_free_text_associated($specify,$variables)
    {

        $code_holder = '[ICD code]';
        $name_holder = '['.$specify->additional_text.']';
        $associated_text = '';
        $associated = array();
        $associated_var = (empty($variables->associated) ? '' : $variables->associated);
        if(empty($associated_var) || empty((array) $associated_var)){
            array_push($associated, array('code' => $code_holder, 'text' => $name_holder.';'));
            $associated_text = $name_holder;
        }
        else{
            $len = count((array) $associated_var);
            foreach($associated_var as $k=>$v){
                $code = (empty($v->code) ? $code_holder : $v->code);
                $name = (empty($v->name) ? $name_holder : $v->name);
                if(!empty($associated_text))
                    $associated_text.=', and with ';
                $associated_text .= $name;
                array_push($associated,array('code'=>$code,'text'=>$name.';'));
            }
        }
        return array($associated,$associated_text);
    }

    private function get_associated_formal($formal,$diagnosis,$patient_diagnosis_id,$variables,$text)
    {
        if(!in_array($diagnosis->id,[13,22,251])){
            $code_holder = '[ICD code]';
            $name_holder = $this->get_associated_place_holder($diagnosis);
            $associated_text = '';
            $associated = array();
            $associated_var = (empty($variables->associated) ? '' : $variables->associated);
            if(empty($associated_var) || empty((array) $associated_var)){
                array_push($associated, array('code' => $code_holder, 'text' => $name_holder.';'));
                $associated_text = $name_holder;
            }
            else{
                $len = count((array) $associated_var);
                foreach($associated_var as $k=>$v){
                    $code = (empty($v->code) ? $code_holder : $v->code);
                    $name = (empty($v->name) ? $name_holder : $v->name);
                    if(!empty($associated_text))
                        // $associated_text = 'associated with ';
                    // else
                        $associated_text.=', and with ';
                    $associated_text .= $name;
                    array_push($associated,array('code'=>$code,'text'=>$name.';'));
                }
            }
            $diagnosis_name = strtolower($diagnosis->name);
            $formal[0]['text'] = strtolower(str_replace($diagnosis_name,$diagnosis_name.' associated with '.$associated_text,$formal[0]['text']));
            $formal = array_merge($associated,$formal);



        }
        return $formal;
    }

    private function get_associated_place_holder($diagnosis)
    {
        $name_holder = '';
        $res = DB::select('select additional_text from '.DB::getTablePrefix().'specifiers where diagnosis_id='.$diagnosis->id.' and variable="associated"');
        if(!empty($res)){
            $name_holder = '['.$res[0]->additional_text.']';
        }
        return $name_holder;
    }

    private function get_catatonia_formal($diagnosis,$cwords)
    {
        $text = '';
        $res=DB::select('select icd  from '.DB::getTablePrefix().'specifiers s inner join '.DB::getTablePrefix().'specifiers_items si on s.id=si.specify_id where behavior_id=5 and var_data="TRUE"');
        if(!empty($res)){
            $code = $res[0]->icd;
            // $text = '; '.$code.' catatonia associated with '.strtolower($diagnosis->name);
            $text = 'catatonia associated with '.strtolower($diagnosis->name);
        }
        // return $text;
        return array('code'=>$code,'text'=>$text);
    }

    private function require_severity($specifier,$variables){
        if($specifier->behavior_name == 'with_use_severity' && !empty($variables->use_disorder) && strtolower($variables->use_disorder) == 'without')
            return false;
        return true;
    }

    private function require_alcohol_type($specifier,$variables){
        if($specifier->behavior_name == 'alcohol_type' && !empty($variables->substance_class) && strtolower($variables->substance_class) != 'alcohol')
            return false;
        return true;
    }
    private function require_bipolar($specifier,$variables,$diagnosis)
    {
        if($diagnosis->behavior=='bipolar'){
            switch ($variables->episode_type) {
                case 'manic_current':
                case 'depressed_current':
                    if(!in_array($specifier->behavior_name,['bi_severity','bi_psychotic']))
                        return false;
                    break;
                case 'manic_recent':
                case 'hypomanic_current':
                case 'hypomanic_recent':
                case 'depressed_recent':
                    if(!in_array($specifier->behavior_name, ['bi_course']))
                        return false;
                    break;
                case 'unspecified':
                    return false;
                    break;
                
            }
        }
        return true;
    }
    
    private function require_bipolar2($specifier,$variables,$diagnosis)
    {
        if($diagnosis->behavior == 'bipolar2'){
            if(in_array($variables->episode_type, ['hypomanic_current','depressed_current']) && !$specifier->behavior_name == 'bi_severity')
                return false;
            elseif (in_array($variables->episode_type, ['hypomanic_recent','depressed_recent']) && $specifier->behavior_name != 'bi_course')
                return false;
        }
        return true;
    }

    private function require_major_depressive($specifier,$variables,$diagnosis)
    {
        if($diagnosis->behavior == 'major_depressive'){
            if($variables->current == 'yes' && !in_array($specifier->behavior_name,['bi_severity','bi_psychotic']))
                return false;
            elseif ($variables->current == 'no' &&  !in_array($specifier->behavior_name,['bi_course']))
                return false;
        }
        return true;        
    }

    private function require_delirium($specifier,$variables,$diagnosis){
        if($diagnosis->behavior == 'delirium'){
            switch ($variables->delirium_whether) {
                case 'substance intoxication delirium':
                    if(!in_array($specifier->behavior_name,array('substance_name','substance_use_disorder','with_use_severity','substance_input','s12','s13')))
                        return false;
                    break;
                case 'substance withdrawal delirium':
                    if(!in_array($specifier->behavior_name, array('small_substance_name','substance_use_disorder','with_use_severity','substance_input','s12','s13')))
                        return false;
                    break;
                case 'medication-induced delirium':
                    if(!in_array($specifier->behavior_name, array('prescribed_medication','medication_name','s12','s13')))
                        return false;
                    break;
                case 'delirium due to another medical condition':
                    if(!in_array($specifier->behavior_name, array('other_medical','s12','s13')))
                        return false;
                    break;
                case 'delirium due to multiple etiologies':
                    if(!in_array($specifier->behavior_name, array('multiple_etiologies','s12','s13')))
                        return false;
                    break;
            }
        }
        return true;
    }

    private function get_parent($specify){
        $behavior_name = $specify->behavior_name;
        $specify = Specify::find($specify->id);
        $specify->get_parent();
        $specify->behavior_name=$behavior_name;
        return $specify;

    }
    private function get_patient_diagnosis_variables($diagnosis,$patient_diagnosis_id)
    {
        $patient_diagnosis_variables = DB::select('select * from '.DB::getTablePrefix().'patient_diagnosis_variables where patient_diagnosis_id='.$patient_diagnosis_id. ' and diagnosis_id='.$diagnosis->id.' and (status is null or status<>"related")');
        $variables = $patient_diagnosis_variables ? $patient_diagnosis_variables[0]->selections : "{}";
        $variables = json_decode($variables);
        return $variables;
    }
    private function capitalize_words()
    {
        $words = array(
            "alzheimer's" => "Alzheimer's",
            "lewy body" => "Lewy body",
            "hiv" => "HIV",
            "parkinson's" => "Parkinson's",
            "huntington's" => "Huntington's",
            "alzheimer’s" => "Alzheimer's",
            "parkinson’s" => "Parkinson's",
            "huntington’s" => "Huntington's"
            );
        return $words;
    }

    // private function ncd_icd_text($diagnosis,$patient_diagnosis_id)
    // {
    //     $text = '';
    //     $specifiers = $diagnosis->specifiers;
    //     if(!empty($specifiers) && count($specifiers)>0)
    //     {
    //         $variables = $this->get_patient_diagnosis_variables($diagnosis,$patient_diagnosis_id);
    //         $specifiers = DB::select('select s.id,s.diagnosis_id,s.title,s.text,s.groups,s.is_require,s.variable,s.parent,b.name as behavior_name from specifiers s left join behaviors b on s.behavior_id=b.id  where s.diagnosis_id='.$diagnosis_id);
    //         foreach ($specifiers as $key => $specifier) {
    //             $var = $specifier->variable;
    //             if($specifier->parent)
    //                 $specifier = $this->get_parent($specifier);
    //             $var = $specifier->variable;
    //             $selections = $variables->$var ?? null;
    //             if($specifier->is_require && empty($selections)){
    //                 return 'missing';
    //             }
    //             else{
    //                 $var_items = NULL;
    //                 if(!empty($selections)){
    //                     if(gettype($selections) == 'string')
    //                         $var_items = addslashes($selections);
    //                     elseif (gettype($selections) == 'array')
    //                         $var_items = join("','",$selections);
    //                 }
    //                 $specifiers_items = DB::select("select * from specifiers_items where specify_id=".$specifier->id." and var_data in('$var_items') order by specify_id");
    //                 foreach ($specifiers_items as $k => $v) {
    //                     if(!empty($v->icd_text)){
    //                         if(!empty($v->icd))
    //                             $code = $v->icd; 
    //                         $text .= ', '.$v->icd_text;  
    //                     }

    //                 }
    //             }

    //         }
    //     }
    //     return  $text;
    // }
}
