<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use App\Criteria;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Diagnosis;
use Response;

class UsersController extends Controller
{
	public function update(Request $request, $id)
	{
		$user = User::find($request->id);
		$user->present_favorite = $request->present_favorite;
		$user->save();
		return response()->json([
		    'present' => $user->present_favorite
		]);
		//return redirect()->action('CriteriaController@index', ['_METHOD' =>'GET', 'id' => $request->diagnosis_id]);

	}
}
