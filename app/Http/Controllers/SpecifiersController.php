<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Criteria;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Diagnosis;
use App\DiagnosisSteps;
use App\PatientDiagnosis;
use App\Specify;
use App\PresentType;
use App\SpecifiersItem;
use App\PatientSpecifiers;
use App\PatientDiagnosisVariables;
use Illuminate\Support\Facades\DB;
use App\History;



class SpecifiersController extends Controller
{

    public function show(Request $request, $diagnosis_id, $id)
    {
        $params = explode(".", $diagnosis_id);
        $real_diagnosis_id = end($params);
        $diagnosis = Diagnosis::find($real_diagnosis_id);
        $specify = Specify::find($id);
        if($specify->parent)
            $specify->get_parent();
        if(!empty($request->behavior) && $request->behavior == 'free_text'){
            $specify->get_additionals();
        }
        $patient_diagnosis_id = $request->session()->get('patient');
        $variables = $this->get_selections($request,$patient_diagnosis_id,$diagnosis,$specify);
        $next_specify = Specify::where([['diagnosis_id', '=', $real_diagnosis_id],['order_num', '>', $specify->order_num ]])->get()->first();
        $specifiers_items = DB::select('select * from '.DB::getTablePrefix().'specifiers_items where specify_id='.$specify->id.' order by id');
        // if(!empty($request->return_from_substance)){
        //     $variables->use_disorder=$request->result;
        // }
        $present_type = PresentType::find($specify->present_type_id);
        if($present_type->name == 'sub_specify'){
            $specifiers_items = $this->get_nested_specifiers_items($specifiers_items);
        }
        if($diagnosis->behavior=="delirium" && $specify->behavior && $specify->behavior->name != 'delirium'){
            $next_id = $this->delirum_switch($specify,$diagnosis,$patient_diagnosis_id);
            $next_specify = Specify::find($next_id);
        }
        elseif($diagnosis->behavior=='bipolar' && $specify->behavior && $specify->behavior->name != 'bi_episode_type'){
            $next_id = $this->bipolar_switch($specify,$diagnosis,$patient_diagnosis_id);
            $next_specify = Specify::find($next_id);
        }
        elseif($diagnosis->behavior=='major_depressive' && $specify->behavior && !in_array($specify->behavior->name ,['md_single_recurrent','md_current'])){
            $next_id = $this->major_depressive_switch($specify,$diagnosis,$patient_diagnosis_id);
            $next_specify = Specify::find($next_id);
        }
        elseif(!empty($diagnosis->behavior) && $diagnosis->behavior=="circadian_sleep_wake" && !($specify->behavior && $specify->behavior->name != 'circadian_sleep_wake')){
            $next_id = $this->circadian_switch($specify,$diagnosis,$patient_diagnosis_id);
            $next_specify = Specify::find($next_id);
        }
        if($this->jump_next_specifier($specify,$variables)){
            if($next_specify)
                return redirect()->action('SpecifiersController@show', ['diagnosis_id' => $diagnosis_id, 'id' => $next_specify]);
            else
                return redirect()->action('SigmaController@index');
        }

        // if(!empty($specify->behavior) && $diagnosis->behavior=="delirium")
        //     $next_specify = $this->delirum_switch($specify,$diagnosis,$patient_diagnosis_id);
        $request->title = 'Specifiers';
        $history = History::create(['patient_diagnosis_id' => $request->session()->get('patient'), 'title' => $diagnosis->name.' - Specifiers','url' => $request->fullUrl()]);
        return view('specifiers.show', ['specify' => $specify, 'specifiers_items' => $specifiers_items, 'breadcrumbs' =>$params, 'diagnosis' => $diagnosis, 'breadcrumbs_str' => $diagnosis_id, 'next_specify' => (!empty($next_specify) ? $next_specify->id : ''),'present_type' => $present_type, 'behavior' => (empty($request->behavior) ? false : $request->behavior),'return_to' => $request->return_to ?  $request->return_to : '', 'variables' => $variables, 'layout' => !$request->ajax()]);
    }

    public function store(Request $request)
    {
        $params = explode(".", $request->diagnosis_id);
        $real_diagnosis_id = end($params);
        $d = Diagnosis::find($real_diagnosis_id);
        $patient_diagnosis_id = $request->session()->get('patient');
        $patient_diagnosis = PatientDiagnosis::find($patient_diagnosis_id);
        $specify = Specify::find($request->specify_id);
        $next_specify = (!empty($request->next_specify) ? $request->next_specify : NULL); 
        $status = (empty($request->return_to) ? NULL : NULL);//"related"
        $specify->save_variables($request->variables,$patient_diagnosis_id,$d->id, $status);
        if($this->is_display_free_text($request))
            return redirect()->action('SpecifiersController@show', ['diagnosis_id' => $request->diagnosis_id, 'id' => $specify->id, 'behavior' => 'free_text']);

        if(!empty($request->return_to) && !(!empty($request->behavior) && $request->behavior=='use_substance')){
            return redirect($request->return_to."?return_from_catatonia=true");
        }
        if(!empty($request->behavior) && $request->behavior=='use_substance'){
            if($next_specify)
                return redirect()->action('SpecifiersController@show', ['diagnosis_id' => $request->diagnosis_id, 'id' => $next_specify, 'return_to' => $request->return_to, 'result'=>'With','substance_name'=>strtolower($d->name),'behavior' => 'use_substance']);              
            else{
                return redirect($request->return_to."?return_from_substance=true&result=With&substance_name=".strtolower($d->name));                
            }

        }
        if(!empty($specify->behavior) && $specify->behavior->name=="delirium"){
            $next_specify = $this->delirum_switch($specify,$d,$patient_diagnosis_id);
        }
        elseif(!empty($specify->behavior) && $specify->behavior->name=="circadian_sleep_wake"){
            $next_specify = $this->circadian_switch($specify,$d,$patient_diagnosis_id);
        }
        elseif (!empty($specify->behavior) && $specify->behavior->name == 'bi_episode_type') {
            $next_specify = $this->bipolar_switch($specify,$d,$patient_diagnosis_id);
        }
        elseif (!empty($specify->behavior) && $specify->behavior->name == 'md_current') {
            $next_specify = $this->major_depressive_switch($specify,$d,$patient_diagnosis_id);
        }
        if($next_specify){
            return redirect()->action('SpecifiersController@show', ['diagnosis_id' => $request->diagnosis_id, 'id' => $next_specify ]);
        }
        elseif(strpos($d->behavior,"mild_type") !== false || strpos($d->behavior,"major_type") !==false){
           $specifiers = DB::select('select s.id from '.DB::getTablePrefix().'specifiers s left join '.DB::getTablePrefix().'behaviors b on s.behavior_id=b.id where s.diagnosis_id='.$d->parent.' and (b.name is null or b.name <> "ncd_types") order by order_num');
            if(!empty($specifiers)){
                $specify_id = $specifiers[0]->id;
                $breadcrumbs = $params;
                array_pop($breadcrumbs);
                $parent_crumbs = implode('.',$breadcrumbs);
                return redirect()->action('SpecifiersController@show', ['diagnosis_id' => $parent_crumbs, 'id' => $specify_id]);
            }
 
        }        return redirect()->action('SigmaController@index');

    }

    public function is_display_free_text($request)
    {
        $specify = Specify::find($request->specify_id);
        if($specify->behavior && $specify->behavior->name == 'free_text_if_yes' && empty($request->behavior))
        {
            $s_var = $specify->variable;
            $values = json_decode($request->variables);
            if(!empty($values->$s_var->var_data) && $values->$s_var->var_data=="Yes"){
                    return true;            
            }
        }
        return false;
    }

    public function  least_one_chosen(Request $request)
    {
        $patient_diagnosis_id = $request->session()->get('patient');
        $specify = Specify::find($request->specify_id);
        $specifiers = DB::select("select * from ".DB::getTablePrefix()."specifiers  where diagnosis_id=".$specify->diagnosis_id." and groups='".$specify->groups."'");
        $patient_diagnosis_variables = PatientDiagnosisVariables::where([['patient_diagnosis_id', "=", $patient_diagnosis_id], ['diagnosis_id', "=", $specify->diagnosis_id]])->get()->first();
        if($patient_diagnosis_variables){
            $variables_obj = json_decode($patient_diagnosis_variables->selections ??  "{}");
            foreach ($specifiers as $key => $value) {
                $s_var = $value->variable;
                if(!empty($variables_obj->$s_var))
                    return response()->json(['one_chosen' => true]);
            }
        }
        return response()->json(['one_chosen' => false]);
    }

    private function get_nested_specifiers_items($specifiers_items)
    {
        $refs = array();
        $list = array();
        foreach ($specifiers_items as $row)
        {
            $ref = & $refs[$row->id];
            $ref['parent'] = $row->parent;
            $ref['text']      = $row->text;
            $ref['var_data'] = $row->var_data;

            if ($row->parent == -1)
            {
                $list[$row->id] = & $ref;
            }
            else
            {
                $refs[$row->parent]['children'][$row->id] = & $ref;
            }
        }
        return $list;
    }


    private function get_selections($request,$patient_diagnosis_id,$diagnosis,$specify)
    {   
        $status = (empty($request->return_to) ? ' and (status is null or status<>"related")' : '');//'status="related"'
        $p_variables = DB::select('select * from '.DB::getTablePrefix().'patient_diagnosis_variables where patient_diagnosis_id='.$patient_diagnosis_id. ' and diagnosis_id='.$diagnosis->id.$status);
        $variables = $p_variables ? $p_variables[0]->selections : "{}";
        $variables = json_decode($variables);
        $behavior_name = $specify->behavior ?  $specify->behavior->name : '';
        $s_var = $specify->variable;
        if(!empty($request->return_from_catatonia)){
            $is_catatonia = $specify->catatonia_result();
            $variables->$s_var = $is_catatonia;
        }
        elseif(!empty($request->return_from_ncd)){
            $variables->type= (!empty($request->result) ? $request->type : "");
        }
        elseif($behavior_name == 'ncd_types' && !empty($variables->type)){
            $type_diagnosis = Diagnosis::where(['parent' => $diagnosis->id, 'behavior' => $variables->type])->first();  
            if(!empty($type_diagnosis)){
                if(!$type_diagnosis->get_diagnostic_indication($patient_diagnosis_id))
                    $variables->type="";
            }
        }
        elseif(!empty($request->return_from_substance)){
            $variables->use_disorder=$request->result;
        }
        elseif(($behavior_name == 'calculated_severity' || $behavior_name == 'with_use_severity') && empty($variables->severity)){
            if($behavior_name == 'calculated_severity'){
                $query = 'select count(*) num from '.DB::getTablePrefix().'diagnosis_steps ds inner join '.DB::getTablePrefix().'criteria c on c.id=ds.criterion_id and ds.patient_diagnosis_id='.$patient_diagnosis_id.' where c.parent in(select c1.id from '.DB::getTablePrefix().'criteria c1 where (c1.parent=0 or c1.parent IS NULL ) and c1.diagnosis_id='.$diagnosis->id.') and ds.result = "Yes" '.$status;
            }
            else{
                $substance_use_disorder = $variables->substance_class.' use disorder';
                $query = 'select count(*) num from '.DB::getTablePrefix().'diagnosis_steps ds inner join '.DB::getTablePrefix().'criteria c on c.id=ds.criterion_id and ds.patient_diagnosis_id='.$patient_diagnosis_id.' inner join '.DB::getTablePrefix().'diagnosis d on d.id=c.diagnosis_id where c.parent in(select c1.id from '.DB::getTablePrefix().'criteria c1 inner join '.DB::getTablePrefix().'diagnosis d1 on c1.diagnosis_id=d1.id where (c1.parent=0 or c1.parent IS NULL ) and LOWER(d.name)="'.strtolower($substance_use_disorder).'") and ds.result="Yes" and ds.status="related"';
            }
            $res = DB::select($query);
            if(!empty($res)){
                $num = $res[0]->num;
                if ($num<4)
                    $variables->severity = 'Mild';
                elseif ($num<6)
                    $variables->severity ='Moderate';
                else
                    $variables->severity = 'Severe';
            }        
        }
        return $variables;
    }

    private function conditional_specifiers()
    {
        return array(
            'with_use_severity',
            'alcohol_type',
            'with_disorder_of',
            'behavioral_input',
            'bi_psychotic2',
            'sleep_type',
            'psycho_stressor'
            );
    }

    private function jump_next_specifier($specify,$variables)
    {
        Log::info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ BEHAVIOR_ID= ".$specify->behavior_id);
        if(!empty($specify->behavior) && in_array($specify->behavior->name,$this->conditional_specifiers())){
            if(empty($variables))
                return true;
            switch ($specify->behavior->name) {
                case 'with_use_severity':
                    if(empty($variables->use_disorder) || strtolower($variables->use_disorder) == 'without')
                        return true;
                    break;
                case 'alcohol_type':
                    if(empty($variables->substance_class) || strtolower($variables->substance_class) != "alcohol")
                        return true;
                    break;
                case 'with_disorder_of':
                    if(empty($variables->with_disorder_of))
                        return true;
                    break;
                case 'behavioral_input':
                    if(empty($variables->behavioral) || strtolower($variables->behavioral) == 'without')
                        return true;
                    break;
                case 'bi_psychotic2':
                    if(empty($variables->psychotic) || strtolower($variables->psychotic == 'no'))
                        return true;
                    break;
                case 'sleep_type':
                    if(empty($variables->sleep_type) || strtolower($variables->sleep_type == 'terror'))
                        return true;
                    break;
                case 'psycho_stressor':
                Log::info("<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                    if(empty($variables->with_psy_stressor) || strtolower($variables->with_psy_stressor == 'without'))
                        return true;
                    break;
            }
        }
        return false;
    }
    private function major_depressive_switch($specify,$diagnosis,$patient_diagnosis_id)
    {
        $patient_diagnosis_variables = PatientDiagnosisVariables::where([['patient_diagnosis_id', "=", $patient_diagnosis_id], ['diagnosis_id', "=", $diagnosis->id]])->get();
        $variables = $patient_diagnosis_variables ? $patient_diagnosis_variables[0]->selections : "{}";
        $variables = json_decode($variables);
        $behavior_name = $specify->behavior ?  $specify->behavior->name : '';
        // if(!empty($variables->episode_type)){
            switch ($behavior_name) {
                case 'md_single_recurrent':
                    $next = 'md_current';
                    break;
                case 'md_current':
                    if(!empty($variables->current) && $variables->current=='yes')
                        $next='bi_severity';
                    else
                        $next = "bi_course";
                    break;
                case 'bi_severity':
                    $next = 'bi_psychotic';
                    break;
                case 'bi_psychotic':
                    $next = 'bi_psychotic2';
                    break;
                case 'bi_psychotic2':
                    $next = 'bi_anxious_distress';
                    break;
                case 'bi_course':
                    $next = 'bi_anxious_distress';
                    break;
                case 'bi_anxious_distress':
                    $next = 'bi_mixed_depressive';
                    break;
                case 'bi_mixed_depressive':
                    $next = 'bi_s9';
                    break;
                case 'bi_s9':
                    $next = 'bi_atypical_features';
                    break;
                case 'bi_atypical_features':
                    $next = 'bi_with_catatonia';
                    break;
                case 'bi_with_catatonia':
                    $next = 'bi_prepartum_onset';
                    break;
                case 'bi_prepartum_onset':
                    if($variables->single_recurrent == 'recurrent')
                        $next = 'bi_s14';
                    else
                        $next =null;
                    break;
                case 'bi_s14':
                    $next=null;
                    break;
            }
            $q="select s.id from ".DB::getTablePrefix()."specifiers s left join ".DB::getTablePrefix()."behaviors b on s.behavior_id=b.id where diagnosis_id=".$diagnosis->id." and b.name='".$next."'";
            $res=DB::select($q);
            if(!empty($res))
                return $res[0]->id;
        // }
        return null;
    }
    private function bipolar_switch($specify,$diagnosis,$patient_diagnosis_id)
    {
        $patient_diagnosis_variables = PatientDiagnosisVariables::where([['patient_diagnosis_id', "=", $patient_diagnosis_id], ['diagnosis_id', "=", $diagnosis->id]])->get();
        $variables = $patient_diagnosis_variables ? $patient_diagnosis_variables[0]->selections : "{}";
        $variables = json_decode($variables);
        $behavior_name = $specify->behavior ?  $specify->behavior->name : '';
        if(!empty($variables->episode_type)){
            switch ($behavior_name) {
                case 'bi_episode_type':
                    if($diagnosis->behavior =='bipolar'){
                        if(in_array($variables->episode_type,['manic_current','depressed_current']))
                            $next = 'bi_severity';
                        elseif(in_array($variables->episode_type, ['manic_recent','hypomanic_recent','depressed_recent','hypomanic_current']))
                            $next = 'bi_course';
                        elseif ($variables->episode_type =='unspecified')
                            $next = 'bi_anxious_distress';

                    }
                    elseif($diagnosis->behavior =='bipolar2'){
                        if(in_array($variables->episode_type,['hypomanic_current','depressed_current']))
                            $next = 'bi_severity'; 
                        elseif(in_array($variables->episode_type, ['hypomanic_recent','depressed_recent']))
                            $next =  'bi_course';                   
                    }
                    break;
                case 'bi_severity':
                    $next = 'bi_psychotic';
                    break;
                case 'bi_psychotic':
                    $next = 'bi_psychotic2';
                    break;
                case 'bi_psychotic2':
                    $next = 'bi_anxious_distress';
                    break;
                case 'bi_course':
                    if($diagnosis->behavior =="bipolar")
                        $next = 'bi_anxious_distress';
                    elseif($diagnosis->behavior=='bipolar2' && in_array($variables->episode_type, ['hypomanic_recent','depressed_recent']))
                        $next ='bi_psychotic';
                    break;
                case 'bi_anxious_distress':
                    if(in_array($variables->episode_type,['manic_current','hypomanic_current']))
                        $next = 'bi_mixed_manic_hypomanic';
                    else{
                        if($variables->episode_type == 'depressed_current')
                            $next = 'bi_mixed_depressive';
                        else
                            $next = 'bi_s9';
                    }
                    break;
                case 'bi_mixed_manic_hypomanic':
                case 'bi_mixed_depressive':
                    $next = 'bi_s9';
                    break;
                case 'bi_s9':
                    $next = 'bi_s10';
                    break;
                case 'bi_s10':
                    if(in_array($variables->episode_type, ['depressed_current','depressed_recent']))
                        $next = 'bi_atypical_features';
                    else{
                        if(in_array($variables->episode_type, ['manic_current','manic_recent']))
                            $next = 'bi_with_catatonia';
                        else
                            $next = 'bi_prepartum_onset';
                    }
                    break;
                case 'bi_atypical_features':
                    if(in_array($variables->episode_type, ['manic_current','manic_recent','depressed_current','depressed_recent']))
                        $next = 'bi_with_catatonia';
                    else
                        $next = 'bi_prepartum_onset';
                    break;
                case 'bi_with_catatonia':
                    $next = 'bi_prepartum_onset';
                    break;
                case 'bi_prepartum_onset':
                    $next = 'bi_s14';
                    break;
                case 'bi_s14':
                    $next=null;
                    break;
            }
            $q="select s.id from ".DB::getTablePrefix()."specifiers s left join ".DB::getTablePrefix()."behaviors b on s.behavior_id=b.id where diagnosis_id=".$diagnosis->id." and b.name='".$next."'";
            $res=DB::select($q);
            if(!empty($res))
                return $res[0]->id;
        }
        return null;
    }

    private function delirum_switch($specify,$diagnosis,$patient_diagnosis_id)
    {
        $patient_diagnosis_variables = PatientDiagnosisVariables::where([['patient_diagnosis_id', "=", $patient_diagnosis_id], ['diagnosis_id', "=", $diagnosis->id]])->get();
        $variables = $patient_diagnosis_variables ? $patient_diagnosis_variables[0]->selections : "{}";
        $variables = json_decode($variables);
        $behavior_name = $specify->behavior ?  $specify->behavior->name : '';
        if(!empty($variables->delirium_whether)){
            switch ($variables->delirium_whether) {
                case 'substance intoxication delirium':
                    $flow=$this->delirium_flow(0);
                    break;
                case 'substance withdrawal delirium':
                    $flow=$this->delirium_flow(1);
                    break;
                case 'medication-induced delirium':
                    $flow=$this->delirium_flow(2);
                    break;
                case 'delirium due to another medical condition':
                    $flow=$this->delirium_flow(3);
                    break;
                case 'delirium due to multiple etiologies':
                    $flow=$this->delirium_flow(4);
                    break;
                default:
                    $flow=[];
                    break;

            }
            if(!empty($specify->behavior) && $specify->behavior->name=='delirium')
                $idx=-1;
            else
                $idx = array_search($behavior_name, $flow);
            if($idx+1 < count($flow)){
                $next = $flow[$idx+1];
                $q="select s.id from ".DB::getTablePrefix()."specifiers s left join ".DB::getTablePrefix()."behaviors b on s.behavior_id=b.id where diagnosis_id=".$diagnosis->id." and b.name='".$next."'";
                $res=DB::select($q);
                if(!empty($res))
                    return $res[0]->id;

            }

        }
        return null;
    }

    private function delirium_flow($idx){
        $arr_delirium =array(
            0=>array('substance_name','substance_use_disorder','with_use_severity','substance_input','s12','s13'),
            1=>array('small_substance_name','substance_use_disorder','with_use_severity','substance_input','s12','s13'),
            2=>array('prescribed_medication','medication_name','s12','s13'),
            3=>array('other_medical','s12','s13'),
            4=>array('multiple_etiologies','s12','s13')
        );
        return $arr_delirium[$idx];
    }
    private function circadian_switch($specify,$diagnosis,$patient_diagnosis_id)
    {
        $patient_diagnosis_variables = PatientDiagnosisVariables::where([['patient_diagnosis_id', "=", $patient_diagnosis_id], ['diagnosis_id', "=", $diagnosis->id]])->get();
        $variables = $patient_diagnosis_variables ? $patient_diagnosis_variables[0]->selections : "{}";
        $variables = json_decode($variables);
        $variable = $specify->variable;
        $flow =array();
        if(!empty($variables->circadian_whether)){
            switch ($variables->circadian_whether) {
                case 'delayed':
                    $flow=array('s2','s3','s5');
                    break;
                case 'advanced':
                    $flow=array('s4','s5');
                    break;
                default:
                    $flow=array('s5');
                    break;
            }
            if(!empty($specify->behavior) && $specify->behavior->name=='circadian_sleep_wake')
                $idx=-1;
            else
                $idx = array_search($variable, $flow);
            if($idx+1 < count($flow)){
                $next = $flow[$idx+1];
                $q="select s.id from ".DB::getTablePrefix()."specifiers s where diagnosis_id=".$diagnosis->id." and variable='".$next."'";
                $res=DB::select($q);
                if(!empty($res))
                    return $res[0]->id;
            }

        }
        return null;
    }
}