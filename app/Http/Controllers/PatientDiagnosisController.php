<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\PatientDiagnosis;


class PatientDiagnosisController extends Controller
{
    public function create_patient_session(Request $request){
        $patient_id = (in_array($request->patient_id, array(1,2,3,4,5)) ? $request->patient_id : 1);
        $patient_diagnosis = PatientDiagnosis::where(['patient_id' => $patient_id])->first();
        $request->session()->put('patient', $patient_diagnosis->id);
        return response()->json([
            'patient_id' => $request->session()->get('patient')
        ]);
    }



}