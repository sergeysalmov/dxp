<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use App\Specify;
use App\SpecifiersItem;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\CriteriaController;
use Illuminate\Support\Facades\DB;

class ListController extends Controller
{
    public function get_list(Request $request)
    {
        $all_diagnosis = Diagnosis::orderBy('name')->get();
        //$all_diagnosis = Diagnosis::all();
        $list = array();
        foreach ($all_diagnosis as $diagnos){
          if (!empty($diagnos->ICD10))
            array_push($list,$diagnos);
          elseif (empty(Diagnosis::where('parent',$diagnos->id)->first())){
            $specifiers = Specify::where('diagnosis_id',$diagnos->id)->get();
            if ($specifiers->isEmpty()){
              $diagnos->ICD10 = 'as specifier';
              array_push($list,$diagnos);
            }
            else{
              foreach ($specifiers as $specify) {
                $specify_items = SpecifiersItem::where('specify_id',$specify->id)->get();
                foreach ($specify_items as $specify_item) {
                    $diagnos_for_list = new Diagnosis;
                    $diagnos_for_list->name = $diagnos->name.', '.$specify_item->icd_text;
                    if (empty($specify_item->icd)) $diagnos_for_list->ICD10 = 'as specifier';
                    else $diagnos_for_list->ICD10 = $specify_item->icd;
                    $diagnos_for_list->id = $diagnos->id;
                    $diagnos_for_list->specify = $specify ->id;
                    $diagnos_for_list->specify_item = $specify_item ->id;
                    array_push($list,$diagnos_for_list);
                }
              }
            }
          }
        }
        return $list;
    }

}
