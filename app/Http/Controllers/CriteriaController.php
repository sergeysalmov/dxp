<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Criteria;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Diagnosis;
use App\User;
use Response;
use Illuminate\Support\Facades\DB;
use App\History;


class CriteriaController extends Controller
{

    public function index(Request $request, $id)
    {
        $user = User::current_user();
        $user_favorite = ($user->present_favorite ?? 'one');//'all';//
        $params = explode(".", $id);
        $diagnosis_id = end($params);
        $diagnosis = Diagnosis::find($diagnosis_id);
        $patient_diagnosis_id = $request->session()->get('patient');
        if(in_array($diagnosis->behavior,["major_ncd","major_type","mild_ncd","mild_type","major_type;substance_use","mild_type;substance_use"]))
            $status_condition='';
        elseif(empty($request->return_to))
            $status_condition = 'and (ds.status is null or ds.status = "")';
        else
            $status_condition = 'and ds.status="related"';
        $criteria_list = DB::select('select c.*,ds.result,ds.free_text from '.DB::getTablePrefix().'criteria c left join '.DB::getTablePrefix().'diagnosis_steps ds on ds.patient_diagnosis_id='.$patient_diagnosis_id.' and c.id=ds.criterion_id '.$status_condition.' where c.diagnosis_id='.$diagnosis_id);
        // $criteria_list = DB::table('criteria')
        //     ->leftJoin('diagnosis_steps', function($join) use($patient_diagnosis_id,$ds_status){
        //             $join->on('diagnosis_steps.patient_diagnosis_id', '=', DB::raw("'".$patient_diagnosis_id."'"));
        //             $join->on('criteria.id', '=', 'diagnosis_steps.criterion_id');
        //             $join->on('diagnosis_steps.status', '=', $ds_status); })
        //     ->select('criteria.*', 'diagnosis_steps.result', 'diagnosis_steps.free_text')
        //     ->where('criteria.diagnosis_id', '=', $diagnosis_id)
        //     ->get();
        $nested_criteria = $this->get_nested_criteria($criteria_list,$id,$patient_diagnosis_id,$diagnosis);
        $next_criterion='';
        $criterion_page = 0;
        if($user_favorite == "one"){
            if(!empty($request->criterion_id) && !empty($nested_criteria[$request->criterion_id])){
                $key = $request->criterion_id;
                if(isset($request->criterion_page)){
                    $criterion_page = $request->criterion_page+1;
                }
            }
            else
                $key = key($nested_criteria);
            $one_criterion = array($key => $nested_criteria[$key]);
            $parent_keys = array_keys($nested_criteria);
            $next_criterion = current(array_slice($parent_keys,array_search($key, $parent_keys)+1));
            $nested_criteria = $one_criterion;
        }
        $request->title = $diagnosis->name;
        $history = History::create(['patient_diagnosis_id' => $request->session()->get('patient'), 'title' => $diagnosis->name.' - Criteria','url' => $request->fullUrl()]);
        return view('criteria.index', ['criteria' => $criteria_list, 'layout' => !$request->ajax(),"diagnosis" => $diagnosis, 'breadcrumbs' => $params, 'nested_criteria' => $nested_criteria, 'present' => $user_favorite, 'next_criterion' => $next_criterion, 'criterion_page' => $criterion_page, 'breadcrumbs_str' => $id,  'return_to' => $request->return_to ?  $request->return_to : "", 'return_type' => $request->return_type ? $request->return_type : '', 'nested' => $request->nested ? $request->nested : '']);
    }


    private function get_nested_criteria($criteria_list,$breadcrumbs,$patient_diagnosis_id,$diagnosis)
    {
        $refs = array();
        $list = array();
        foreach ($criteria_list as $row)
        {
            $ref = & $refs[$row->id];
            $ref['parent'] = $row->parent;
            $ref['name']      = $row->name;
            $ref['behavior'] = $row->behavior;
            if($row->behavior == 'see_m_ncd' && empty($row->result)){
                $ref['result'] = $diagnosis->m_ncd_result($patient_diagnosis_id);
            }
            else
                $ref['result'] = $row->result;
            $ref['free_text'] = $row->free_text;

            if ($row->parent == 0)
            {
                $list[$row->id] = & $ref;
            }
            else
            {
                $refs[$row->parent]['children'][$row->id] = & $ref;
            }
        }
        return $list;
    }

}