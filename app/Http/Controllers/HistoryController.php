<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Criteria;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Diagnosis;
use App\History;
use Response;

class HistoryController extends Controller
{
	public function store(Request $request)
	{
		$history = new History;
        $history->patient_diagnosis_id = $request->session()->get('patient');
	    $history->title = $request->title;
        $history->url = $request->url;
        $history->save();

        return response()->json([
		    'name' => 'Abigail',
		    'state' => 'CA'
		]);
	}
}
