<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use App\Criteria;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\CriteriaController;
use App\DiagnosisSteps;
use App\History;
use Illuminate\Support\Facades\DB;
use Cookie;
use Crypt;

class DiagnosisController extends Controller
{
    protected $disorder_full_list;
    public function __construct()
    {
        $this->disorder_full_list = Diagnosis::cached_all();
    }


    public function index(Request $request)
    {   
        if(empty($request->session()->get('patient')))
            $request->session()->put('patient', '1');
        $request->title = 'Classification cluster';
        $history = History::create(['patient_diagnosis_id' => $request->session()->get('patient'), 'title' => 'Classification cluster','url' => $request->fullUrl()]);
        $template = Cookie::get('diagnose-view') == 'list' ? 'diagnosis.index-list' : 'diagnosis.index';
        return view ($template, ['diagnosis' => $this->disorder_full_list, 'layout' => !$request->ajax()]);

    }

    public function show(Request $request, $id)
    {
        $params = explode(".", $id);
        $diagnosis = Diagnosis::find(end($params));
        $d = $this->disorder_full_list;
        $params_length = count($params);
        for($i=0; $i < $params_length; $i++){
            $d = $d[$params[$i]];
            if($params_length>$i+1)
                $d=$d['children'];
        }
        if(empty($d['children'])){
            return redirect()->action('CriteriaController@index', ['id' => $id]);
        }
        $request->title = $d['name'];
        $history = History::create(['patient_diagnosis_id' => $request->session()->get('patient'), 'title' => $d['name'],'url' => $request->fullUrl()]);
        return view('diagnosis.show',['diagnosis_obj' => $d, 'layout' => !$request->ajax(), 'level' =>$id, 'breadcrumbs' => $params,'diagnosis' => $diagnosis]);
    }

    public function change_view(Request $request){
        Cookie::queue('diagnose-view',$request->view);
        return redirect('/diagnosis');
    }

//     public function get_disorders($diagnosis_result)
//     {
//         $refs = array();
//         $list = array();

// //         $sql = "SELECT item_id, parent_id, name FROM items ORDER BY name";

// // /** @var $pdo \PDO */
// // $result = $pdo->query($sql);

//         foreach ($diagnosis_result as $row)
//         {
//             $ref = & $refs[$row->id];
//             $ref['parent'] = $row->parent;
//             $ref['name']      = $row->name;

//             if ($row->parent == 0)
//             {
//                 $list[$row->id] = & $ref;
//             }
//             else
//             {
//                 $refs[$row->parent]['children'][$row->id] = & $ref;
//             }
//         }
//         return $list;
//     }

}
