<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Criteria;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Diagnosis;
use App\DiagnosisSteps;
use App\PatientDiagnosis;
use App\Specify;
use App\PatientSpecifiers;


class PatientSpecifiersController extends Controller
{
    public function store(Request $request)
    {
        $patient_diagnosis_id = $request->session()->get('patient');
        
        $patient_diagnosis = PatientDiagnosis::find($patient_diagnosis_id);

        // foreach ($request->answers as $key => $value) {
        //     $diagnosis_steps = DiagnosisSteps::where(['patient_diagnosis_id' => $patient_diagnosis->id, 'criterion_id' => $key])->first();
        //     if(!empty($diagnosis_steps)){
        //         if($diagnosis_steps->result != $value){
        //             $diagnosis_steps->result = $value;
        //             $diagnosis_steps->save();
        //         }
        //     }
        //     else{
        //         $diagnosis_steps = new DiagnosisSteps;
        //         $diagnosis_steps->patient_diagnosis_id = $patient_diagnosis->id;
        //         $diagnosis_steps->criterion_id = $key;
        //         $diagnosis_steps->result = $value;
        //         $diagnosis_steps->save();
        //     }
        // }
     
        // if(!empty($request->next_criterion)){
        //     return redirect()->action('CriteriaController@index', ['id' => $request->diagnosis_id, 'criterion_id' => $request->next_criterion, 'criterion_page' => $request->criterion_page]);
        // }
        // else{
        //     $params = explode(".",$request->diagnosis_id);
        //     $real_id = end($params);
        //     $specify = Specify::where('diagnosis_id', $real_id)->first();
        //     if(!empty($specify)){
        //         return redirect()->action('SpecifiersController@show', ['diagnosis_id' => $request->diagnosis_id, 'id' => $specify->id]);
        //     }
        // }
        return redirect()->action('SigmaController@show');

    }




}