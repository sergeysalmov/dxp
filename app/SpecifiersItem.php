<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecifiersItem extends Model
{
   
    protected $table = 'specifiers_items';

    public function specify()
    {
        return $this->belongsTo('App\Specify');
    }

}
