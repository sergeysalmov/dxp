<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\PatientDiagnosisVariables;

class Specify extends Model
{
   
    protected $table = 'specifiers';

    public function diagnosis()
    {
        return $this->belongsTo('App\Diagnosis');
    }

    public function specifiers_items()
    {
        return $this->hasMany('App\SpecifiersItem');
    }

    public function behavior()
    {
        return $this->belongsTo('App\Behavior');
    }

    public function present_type()
    {
        return $this->belongsTo('App\PresentTypes');
    }

    public function get_additionals()
    {
        $additionals = json_decode($this->additionals);

        $this->title = $additionals->inner_title;
        $this->text = $additionals->inner_text;
        $this->present_type_id = 9;
    }

    public function get_parent()
    {
        if(empty($this->behavior) || $this->behavior && $this->behavior->name !== 'show_catatonia_criteria'){
            $parent_specifier = Specify::find($this->parent);
            $this->id =$parent_specifier->id;
            $this->present_type_id =$parent_specifier->present_type_id;
            $this->title =$parent_specifier->title;
            $this->explanation =$parent_specifier->explanation;
            $this->text =$parent_specifier->text;
            $this->additional_text =$parent_specifier->additional_text;
            $this->is_require =$parent_specifier->is_require;
            $this->multi_select =$parent_specifier->multi_select;
            // $this->behavior_id =$parent_specifier->behavior_id;
            $this->info = (empty($this->info) ? $parent_specifier->info : $this->info);
            $this->note = (empty($this->note) ? $parent_specifier->note : $this->note);
            $this->groups =$parent_specifier->groups;
            $this->additionals =$parent_specifier->additionals;
            $this->variable = (in_array($parent_specifier->variable, $this->public_variables()) ? $parent_specifier->variable : $this->variable);
            // $this->behavior_id = $parent_specifier->behavior_id;
            // $this->order_num = $this->order_num;
        }
    }

    public function public_variables(){
        return array(
            'substance_class',
            'use_disorder',
            'substance_input',
            'severity',
            'another_medical',
            'associated',
            'catatonia'
        );
    }
    
    public function get_aub_specify_present()
    {
        $this->present_type_id = 6;
    }

    public  function catatonia_result($diagnosis_id=null)
    {
        $patient_diagnosis_id = session('patient');
        if(empty($diagnosis_id)){
            $catatonia = Specify::find($this->parent);
            $diagnosis_id = $this->diagnosis_id;
        }
        else
            $catatonia = $this;
        $res  = DB::select('select selections from '.DB::getTablePrefix().'patient_diagnosis_variables where patient_diagnosis_id='.$patient_diagnosis_id. ' and diagnosis_id='.$diagnosis_id);   
        $result = (empty($res) ? '[]' : $res[0]->selections);
        $result = json_decode($result);
        $catatonia_var = $catatonia->variable;
        if(!empty($result->$catatonia_var))
            if(in_array('TRUE', $result->$catatonia_var))
                return "Yes";
        return "No";
    }


    public function get_use_disorder($use_disorder)
    { 
        return ($use_disorder ? strtolower($use_disorder). ' use disorder' : ''); 
    }

    public function get_id_by_name($disorder_name)
    {
        $disorder_name = $this->get_name_path($disorder_name);
        $path = '';
        $diagnosis = DB::select("select id,LOWER(name),parent from ".DB::getTablePrefix()."diagnosis where LOWER(name)='".$disorder_name."'");
        if(!empty($diagnosis)){
            $diagnosis = $diagnosis[0];
            $path = $diagnosis->id;
            $parent = $diagnosis->parent;
            while ($parent) {
                $d = DB::select('select id,parent from '.DB::getTablePrefix().'diagnosis where id='.$parent);
                if(!empty($d)){
                    $path = $d[0]->id.'.'.$path;
                    $parent = $d[0]->parent;
                }
            }
        }
        return ( $path.'/criteria');
    }
    
    public function get_name_path($disorder_name)
    {
        if($disorder_name == 'amphetamine (or other stimulant) use disorder' || $disorder_name == 'cocaine use disorder')
            $disorder_name = 'stimulant use disorder';
        return $disorder_name;

    }

    public function save_variables($variables,$patient_diagnosis_id,$diagnosis_id,$status)
    {
        $patient_diagnosis_variables = PatientDiagnosisVariables::where(['patient_diagnosis_id' => $patient_diagnosis_id, 'diagnosis_id' => $diagnosis_id, 'status' => $status])->first();
        if(empty($patient_diagnosis_variables)){
            $patient_diagnosis_variables = new PatientDiagnosisVariables;
            $patient_diagnosis_variables->patient_diagnosis_id = $patient_diagnosis_id;
            $patient_diagnosis_variables->diagnosis_id = $diagnosis_id;
            $patient_diagnosis_variables->status = $status;

        }
        if($patient_diagnosis_variables->selections != $variables){
            $data = json_decode($variables);
            $prev_data = json_decode($patient_diagnosis_variables->selections);
            if($this->behavior && $this->behavior->name == 'substance_name'){
                if(empty($prev_data->substance_class) || $data->substance_class != $prev_data->substance_class){
                    unset($data->use_disorder);
                    unset($data->severity);
                }
            }
            elseif($this->behavior && $this->behavior->name == 'substance_use_disorder' && $data->use_disorder == 'without'){
                unset($data->severity);
            }
            elseif($this->behavior && $this->behavior->name == 'delirium')
                if(!empty($prev_data->delirium_whether) && $prev_data->delirium_whether != $data->delirium_whether){
                    $variables = json_encode(array('delirium_whether'=> $data->delirium_whether));
                }
            $variables = $data;
            $patient_diagnosis_variables->selections = json_encode($variables);
            $patient_diagnosis_variables->save();
        }

    }
    public function ncd_type_url($breadcrumbs_str,$type,$diagnosis)
    {
        $url = '';
        $diagnosis_id = $this->diagnosis_id;
        if($type =='Unspecified Neurocognitive Disorder')
            $diagnosis_id = explode('.',$breadcrumbs_str)[0];
        $name = $this->ncd_type_name($type,$diagnosis);
        $res = DB::select('select id from '.DB::getTablePrefix().'diagnosis where parent ='.$diagnosis_id.' and name="'.$name.'"');
        if(!empty($res)){
            $id = $res[0]->id;
            $url = '/diagnosis/'.$breadcrumbs_str.'.'.$id.'/criteria';
        }
        return $url;
    }
    public function ncd_type_name($type,$diagnosis)
    {
        $name = $type;
        if($type !='Unspecified Neurocognitive Disorder'){
            $kind = ($diagnosis->behavior == 'major_ncd' ? 'Major' : 'Mild');
            if($type == 'Substance/Medication-Induced')
                $name = $type.' '.$kind.' NCD';
            else
                $name = $kind.' '.$type;
        }
        return $name;

    }

}
