<?php

namespace App;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Diagnosis extends Model
{
    //
    protected $table = 'diagnosis';
    public static $disorder_full_list;

    public function criteria()
    {
        return $this->hasMany('App\Criteria');
    }

    public function specifiers()
    {
        return $this->hasMany('App\Specify');
    }

    public function name($id)
    {
    	return $this::find($id)->name;

    }
    public static function cached_all() {
       if (empty(self::$disorder_full_list)) {
       		// $diagnosis_result = self::all();
            $diagnosis_result = self::orderBy('order_num')->orderBy('id')->get();
       		self::$disorder_full_list = self::get_disorders($diagnosis_result);
        }
        return self::$disorder_full_list;
    }

    public static function get_disorders($diagnosis_result)
    {
        $refs = array();
        $list = array();
        foreach ($diagnosis_result as $row)
        {
            $ref = & $refs[$row->id];
            $ref['parent'] = $row->parent;
            $ref['name']      = $row->name;
            $ref['behavior'] = $row->behavior;
            $ref['id'] = $row->id;

            if ($row->parent == 0)
            {
                $list[$row->id] = & $ref;
            }
            else
            {
                $refs[$row->parent]['children'][$row->id] = & $ref;
            }
        }
        return $list;
    }
    public function get_diagnostic_indication($patient_diagnosis_id)
    {
        $criteria = DB::select('select c.id,ds.result from '.DB::getTablePrefix().'criteria c left join '.DB::getTablePrefix().'diagnosis_steps ds on c.id=ds.criterion_id and ds.patient_diagnosis_id='.$patient_diagnosis_id.' where c.diagnosis_id='.$this->id. ' and c.behavior <> "crit_as_title" and (c.parent is NULL or c.parent=0)');
        foreach($criteria as $key => $value){
            if($value->result != "Yes"){
                return 0;
            }
        }
        return 1;
    }

    public function m_ncd_result($patient_diagnosis_id)
    {
        $res = DB::select('select  c.id,ds.result from '.DB::getTablePrefix().'diagnosis d inner join '.DB::getTablePrefix().'criteria c on d.parent=c.diagnosis_id left join  '.DB::getTablePrefix().'diagnosis_steps ds on c.id= ds.criterion_id where d.id ='.$this->id.' and ds.patient_diagnosis_id='.$patient_diagnosis_id.' and c.behavior <> "crit_as_title" and (c.parent is NULL or c.parent=0)');
        if(!empty($res)){
            foreach ($res as $key => $value) {
                if($value->result != "Yes"){
                    return "No";
                }
            }
            return "Yes";
        }
        return null;
    }
    public function slice_name()
    {
        $name = $this->name;
        if($this->name != 'Unspecified Neurocognitive Disorder'){
            $kind = ($this->behavior == 'mild_type' ? 'Mild' : 'Major');
            if($this->name == 'Substance/Medication-Induced')
                $name = trim(str_replace($kind.' NCD', '', $name));
            else
                $name = trim(str_replace($kind, '',$name));
        }
        return $name;
    }

    public function is_other_child()
    {
        if(!empty($this->parent))
            if(Diagnosis::find($this->parent)->behavior == "other_present")
                return true;
        return false;
    }
}
