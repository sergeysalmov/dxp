<?php
namespace App\Helpers;
use Illuminate\Support\Facades\Log;

class IcdCodes {

	public static function induced_psychotic_disorder($substance,$severity)
	{
		$icd_codes = array(
		'Alcohol' => array('mild'=> 'F10.159','moderate_or_severe' => 'F10.259', 'without' => 'F10.959'),
	    'Cannabis' => array('mild'=> 'F12.159','moderate_or_severe' => 'F12.259', 'without' => 'F12.959'),
		'Phencyclidine' => array('mild'=> 'F16.159','moderate_or_severe' => 'F16.259', 'without' => 'F16.959'),
		'Other hallucinogen' => array('mild'=> 'F16.159','moderate_or_severe' => 'F16.259', 'without' => 'F16.959'),
		'Inhalant' => array('mild'=> 'F18.159','moderate_or_severe' => 'F18.259', 'without' => 'F18.959'),
		'Sedative, hypnotic, or anxiolytic' => array('mild'=> 'F13.159','moderate_or_severe' => 'F13.259', 'without' => 'F13.959'),
		'Amphetamine (or other stimulant)' => array('mild'=> 'F15.159','moderate_or_severe' => 'F15.259', 'without' => 'F15.959'),
		'Cocaine' => array('mild'=> 'F14.159','moderate_or_severe' => 'F14.259', 'without' => 'F14.959'),
		'Other (or unknown) substance' => array('mild'=> 'F19.159','moderate_or_severe' => 'F19.259', 'without' => 'F19.959')
		);
		
		return $icd_codes[$substance][$severity];
	}

}
