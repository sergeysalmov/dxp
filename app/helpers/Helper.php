<?php
namespace App\Helpers;
use App\Diagnosis;
use App\Criteria;
use App\Specify;
use Illuminate\Support\Facades\Log;


class Helper {
	public static function renderMenu($items,$level,$type=null) {
		if($items){
		    $render = '<ul  style="padding-left: 20px;">';

		    foreach ($items as $id=>$item) {
		    	$levela = $level .'.' . $id;
		        $render .= '<li class = "disorder_list" id="'.$id.'"  level = "'. $levela .'">';
		        $render .= '<a href="'.
		        ($item['behavior'] == 'disabled' ? '#' : '/diagnosis/'.$levela).
		        '"><span style="font-size:14px;font-family:Tahoma; color: black;">' .$item['name'].'</span></a>';
		        if (!empty($item['children']) && $type != "other_present") {
		            $render .= Helper::renderMenu($item['children'],$level . '.' .$id);
		        }
		        $render .= '</li>';
		    }
		    return $render . '</ul>';
		}
		return '';
	}

	public static function has_children($diagnosis_obj){
		foreach ($diagnosis_obj['children'] as $key=>$val){
			if(empty($val['children']))
				return false;
		}
        return true;
	}

	public static function num_to_alphabetic($num,$level,$letter)
	{
		if($level == 0  && !$letter) return '';
		$level = $level <= 2 ? $level : $level%3;
		$alphabet = array();
		$alphabet[0] = range('A', 'Z');
		$alphabet[1] = range(1, 20);
		$alphabet[2] = range('a', 'z');
		return $alphabet[$level][$num].'.';
	}

	public static function get_breadcrumbs($array){
		$breadcrumbs = "";
		$page = "";
		$length = count($array)-1;
		if($length == 0)
			return '<a href="/diagnosis"><span>Classification Clusters:</span></a>';
		for($i=0;$i<$length;$i++) {
			$page .=$array[$i];
			$breadcrumbs .= '<a href=/diagnosis/' .$page.'><span>' ;
			$breadcrumbs .= Diagnosis::find($array[$i])->name;
			$breadcrumbs .='</span></a><span>';
			$breadcrumbs .= ($length> $i+1) ? ' > </span>' : ': </span>';
			$page .='.';
		}
		return $breadcrumbs;
	}

	public static function render_button($id,$value, $result,$behavior,$class)
	{
		$a_class = "art-button"." $behavior". " $class";
		$checked ='';
		if($value == $result){
			$a_class .=" chosen";
			$checked = "checked";
		}
		return '<a class="'. $a_class .'">'. $value .'</a>
			<input type="radio" name="criterion-' . $id . '" value="' .$value.'" class="hidden-input"' . $checked .'>';
	}

	public static function render_radiobutton($id,$value,$result)
	{
		$label_class = 'art-radiobutton';
		$checked = '';
		if($value == $result){
			$label_class .= ' art-checked';
			$checked = 'checked';
		}
		return '<label class="'.$label_class.'">
                    <span style="font-family: Tahoma; letter-spacing: 1px;">&nbsp;'.$value.'</span>&nbsp;
                </label>
                <input type="radio" name="criterion-' . $id . '" value="' .$value.'" class="hidden-input"' . $checked .'>';
	}

	public static function get_criteria_margin($level,$criterion,$multi_levels)
	{
		if($level == 1){
			if($multi_levels) 
				return "margin: 3px 30px;";		
			else
				return "margin: 3px 50px;";
		}
		else{
			return "margin: 3px ".($level*25)."px;";
		}

	}

	public static function get_border($level,$i,$criterion_num)
	{
		if($level == 0 && $i == $criterion_num)
			return '';
		else
			$class = 'level-'.$level. ($i == 0 ? " first" : " no-first");
		return '<div class="art-content-layout-br border-div '.$class.'"></div>';
	}

	public static function render_criteria($criteria,$level,$criterion_num,$letter,$multi_levels,$x,$breadcrumbs){
		$render = "";
		$i = $level ==0 ? $criterion_num : 0;
		$flag =0;
	    foreach ($criteria as $id=>$criterion) {
	    	$margin = Helper::get_criteria_margin($level,$criterion,$multi_levels);
	    	$border = Helper::get_border($level,$i,$criterion_num);
	    	$prob_pos_class = '';
	    	if($criterion['behavior'] == 'prob_pos_crit'){
	    		$prob_pos_class = 'prob-pos-crit';
	    		if($flag==1){
	    			$i--;
	    			$flag = 0;
	    		}
	    	}
	    	if($criterion['behavior'] == 'probable_possible'){
	    		$render .= Helper::render_probable_crit($id,$criterion,$border,$margin); 
	    		$flag=1;
	    	}
	    	else{
		    	$render .= $border.'<div class="art-content-layout-wrapper" style ="'.$margin.'" id="criterion-panel-'.$id.'">
		    			<div class="art-content-layout criteria-item '.$prob_pos_class.'">
		    				<div class="art-content-layout-row">';
		    					if($criterion['behavior'] != 'crit_as_title')
			    					$render .= '<div class="art-layout-cell criteria-num criteria-num-'.$level.'">
			    					<p><span>'.Helper::num_to_alphabetic($i,$level,$letter) . '</span></p>
			    				</div>';
			    				$render .= '<div class="art-layout-cell criterion-text"><p>'. nl2br(($criterion['name']));
			    				if($criterion['behavior'] == 'see_m_ncd')
			    					$render .= Helper::parent_ncd_link($x,$breadcrumbs);
			    				$render .= '</p></div>';
			    				if($criterion['behavior'] != 'crit_as_title' && $criterion['behavior'] != 'prob_pos_crit')
			    					$render .= '<div id = "'. $id . '" class="art-layout-cell criterion-answers">'.
			    				Helper::buttons($id,$criterion,$level).'
					            </div>';
		       				$render .= '</div>
					    </div>
					</div>';
				}
	        	$i++;
	    	//}
	        if (!empty($criterion['children'])) {
	        	// if($criterion['behavior'] =="parent_prob_pos")
	        	// 	$render .= Helper::render_probable_crit($criterion['children'],$level++,true);
	        	// else{
		        	$level++;
		            $render .= Helper::render_criteria($criterion['children'],$level--,0,$letter,$multi_levels,$x,$breadcrumbs);
		        // }
	        }
	    }

	    return $render;

	}

    public static function parent_ncd_link($x,$breadcrumbs)
    {
        array_pop($breadcrumbs);
        $parent_crumbs = implode('.',$breadcrumbs);
        $return_to = '?return_to='.$x.'&return_type=ncd_type';
        $url = '<span>&nbsp;</span><a href="/diagnosis/'.$parent_crumbs.'/criteria'.$return_to.'">(See criteria)</a>';
        return $url;
    }

	public static function buttons($id,$criterion,$level)
	{
		if($criterion['behavior'] == 'probable_possible')
			$str_buttons = '<p>' . Helper::render_button($id,"probable", $criterion['result'],'probable_possible','') .'<br>'.
								   Helper::render_button($id,"possible", $criterion['result'],'probable_possible','') . '</p>';
		else
		{
			if($level == 0)
				$str_buttons = '<p>' . Helper::render_button($id,"Yes", $criterion['result'],'','') .
								   Helper::render_button($id,"No", $criterion['result'],'','') . '</p><p>' . 
								   Helper::render_button($id,"Uncertain", $criterion['result'],'','') . '</p>';
			elseif ($level == 1)
				$str_buttons = '<p>' . Helper::render_button($id,"Yes", $criterion['result'],'','sub-criterion-button') .
								   Helper::render_button($id,"No", $criterion['result'],'','sub-criterion-button') . '</p>';
			else
				$str_buttons = '<p><span>'.Helper::render_radiobutton($id,"Yes",$criterion['result']) .
									Helper::render_radiobutton($id,"No",$criterion['result']) . '</span></p>';
		}
		return $str_buttons;

	}

	public static function free_text_values($selections)
	{
		$selections = (array) $selections;
		if(!empty($selections))
		{
			$new_values = array_filter($selections, function($k){
				return strstr($k, 'free-text-');
			}, ARRAY_FILTER_USE_KEY);
		}
		if(empty($new_values))
			$new_values = array('free-text-1'=>array('code'=>'','name'=>''));
		return $new_values;
	}

	public static function other_criterion($criteria)
	{
		if(count($criteria)==1 && current($criteria)['behavior'] == 'free_text')
			return true;
		return false;
	}


	public static function group_specifiers_items($specifiers_items)
	{
		$items_group = array();
		foreach($specifiers_items as $key => $value){
			if(empty($items_group[$value->columnA]))
				$items_group[$value->columnA] = array();
			array_push($items_group[$value->columnA], $value);
		}
		return $items_group;
	}

	public static function render_sub_specifiers($specifiers_items,$level,$criterion_num,$letter,$selections)
	{
		$render = "";
		$i = $level ==0 ? $criterion_num : 0;
	    foreach ($specifiers_items as $id=>$s_item) {
	    	$margin = Helper::get_criteria_margin($level,$s_item,true);
	    	$border = Helper::get_border($level,$i,$criterion_num);
	    	$class = $level == 0 ? '' : 'sub-criterion-button';
	    	$a_class = "art-button ".$class;
			$checked ='';
			$selected_arr = ($selections->catatonia ?? []);
			if(in_array($s_item['var_data'], $selected_arr)){
			// if($s_item['result'] == 'Yes'){
				$a_class .=" chosen";
				$checked = "checked";
			}
	    	$render .= $border.'<div class="art-content-layout-wrapper" style ="'.$margin.'">
	    			<div class="art-content-layout criteria-item">
	    				<div class="art-content-layout-row">
		    				<div class="art-layout-cell criteria-num criteria-num-'.$level.'">
		    					<p><span>'.Helper::num_to_alphabetic($i,$level,$letter) . '</span></p>
		    				</div>
		    				<div class="art-layout-cell criterion-text"><p>'. nl2br(e($s_item['text'])) .'</p></div>
		    				<div id = "'. $id . '" class="art-layout-cell specifier-as-criteria-answers">
		    					<p><a  id="'.$id.'" class="'. $a_class .'" var_data="'.$s_item['var_data'].'">'. 'Yes' .'</a>
		    					<input type="checkbox" name="items" value="'.$id.'" class="hidden-input"' . $checked .'></p>
				            </div>
	       				</div>
				    </div>
				</div>';
	        $i++;
	        if (!empty($s_item['children'])) {
	        	$level++;
	            $render .= Helper::render_sub_specifiers($s_item['children'],$level--,0,$letter,$selections);
	        }
	    }
	    return $render;
	}
	public static function count_r($criteria){
		foreach($criteria as $k=>$v){
			if(!empty($v['children'])){
				foreach($v['children'] as $k1=>$v1){
					if(!empty($v1['children']))
						return 1;
				}
			}
		}
		return 0;
	}

	public static function render_probable_crit($id,$criterion,$border,$margin)
	{
		$render=$border.'<div class="art-content-layout-wrapper" style="'.$margin.'">
			<div class="art-content-layout prob_pos_content criteria-item">
			  <div class="art-content-layout-row">
			    <div class="art-layout-cell prob-pos-name" style="width: 75%" >
			      <p class="pic1" style="margin-top: 0in; margin-right: 0in; margin-left: 0in; margin-bottom: 0.0001pt;"><span class="fic1" >
			      '.$criterion['name'].'</span>&nbsp;</p>
			    </div>
			    <div id = "'. $id . '" class="art-layout-cell prob-pos-buttons criterion-answers" style="width: 25%" >
			    <p style="text-align: center;"><span">'.
			    Helper::render_button($id,'Probable',$criterion['result'],'','').
			    Helper::render_button($id,'Possible',$criterion['result'],'','').'</span><br></p>
			    </div>
			    </div>
			  </div>
			</div>';
	    return $render;

	}

	public static function other_columns_present($item)
	{
		foreach($item as $key => $value){
			if(!empty($value['children']))
				return true;
		}
		return false;
	}

	public static function criteria_specifiers($specify)
	{
		if($specify->behavior_id && in_array($specify->behavior->name, ['manic_criteria','hypomanic_criteria','depressive_criteria']))
			return true;
		return false;

	}

}
