<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('/', function(){
        return view('home.index');
    })->name("home");
//Route::group(['prefix' => '{account_id}'], function () {
    Route::view('list', 'list.index');
	Route::resources([
	    'diagnosis' => 'DiagnosisController',
	    'diagnosis.criteria' => 'CriteriaController',
	    'diagnosisSteps' => 'DiagnosisStepsController',
	    'sigma' => 'SigmaController',
	    'history' => 'HistoryController',
	    'diagnosis.specifiers' => 'SpecifiersController',
	    'users' => 'UsersController',
	    // 'patient_diagnosis' => 'PatientDiagnosisController',
	]);
    Route::get('diagnosis_view', 'DiagnosisController@change_view');
	Route::post('diagnosisSteps/store', 'DiagnosisStepsController@store');
	Route::post('specifiers/store', 'SpecifiersController@store');
	// Route::put('users/update', 'UsersController@update');
	Route::get('specifiers/least_one_chosen', 'SpecifiersController@least_one_chosen');
	// Route::get('diagnosis/{id}/criteria', function ($id) {
	//     return redirect('criteria/index');
	// });
	Route::get('diagnosis/{diagnosis}/criteria/{criterion}', function ($diagnosis_id, $criterion_id) {
	    //
	});
	Route::get('diagnosis/{diagnosis}/specifiers/{specify}', function ($diagnosis_id, $specify_id) {
	    //
	});
	Route::get('patient_diagnosis/create_patient_session', 'PatientDiagnosisController@create_patient_session');
//});
// Route::get('{id}/diagnosis', function ($id) {
//     return redirect('diagnosis/show');
// })->where('id', '[0-9]+');
// Route::group(['prefix' => '{id}'], function () {
// 	Route::get('diagnosis', 'DiagnosisController@index');

// });
